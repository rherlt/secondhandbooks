# SecondHandBooks #


### Shortcuts ###

* [Trello Board](https://trello.com/b/7IEropWV/secondhandbooks)

* [Moodlekurs](https://moodle.htw-berlin.de/course/view.php?id=7914)

    * [Kundenwünsche v1.3](https://moodle.htw-berlin.de/pluginfile.php/269116/mod_resource/content/1/kundenwuensche-v1.3.pdf)

    * [technische Anforderungen](https://moodle.htw-berlin.de/pluginfile.php/258272/mod_resource/content/1/technischeAnforderungen.pdf)

* [Wiki (via VPN)](http://wiki-sdf.f4.htw-berlin.de/wiki/Team_D_-_Framework_Spring)

* [Production server](https://ws15sdf-d.f4.htw-berlin.de/secondhandbooks/) (aktuelle Master Build)

* [CI (Jenkins)](https://ws15sdf-d.f4.htw-berlin.de/jenkins/) (Anmeldung mittels HTW-Account)


**Fingerprints des SSL-Zertifikats:**

**UPDATE 2015-12-04:**

* SHA1 Fingerprint=03:06:0B:32:BE:E1:FD:69:52:4D:EF:25:A4:3F:C6:7C:2E:90:0C:A4

* SHA256 Fingerprint=AD:1D:6F:49:86:5B:7A:86:CE:80:44:7C:7D:B0:72:A3:AA:4A:7A:EF:C5:0E:8A:65:DB:B1:2B:6E:17:68:66:6F

* Keine Warnung mehr, kommt jetzt von einer trusted CA


### Installationsanleitung ###

* application.properties anlegen für Datenbankverbindung

    * SecondHandBooks/src/main/resources/application.properties

    * Beispiele in:

    * SecondHandBooks/src/main/resources/application.properties.dist

* application-test.properties anlegen für Datenbankverbindung in JUnit Tests

    * SecondHandBooks/src/test/resources/application-test.properties

    * Beispiele in:

    * SecondHandBooks/src/test/resources/application-test.properties.dist

* SQL Beispieldaten nach erstmaligem Start importieren

    * SecondHandBooks/src/main/resources/sql/data.sql


### Git Regeln ###

Um eine einheitliche Git-Struktur zu haben würde ich gern, dass wir uns auf folgende Punkte für die Nutzung von Git einigen.

* Ein Branch sollte nach der Issue, welches er behandelt, benannt werden

    * "#<IssueNr>_<UseStory>-Englische-Beschreibung-des-Issues"

* Es sollten keine Code verändernden Commits in den Master getätigt werden

* Eine Commitmessage soll mit der Issuenummer beginnen und auf englisch beschreiben was die Änderungen sind

    * "#<IssueNr> Awesome message."

* Pull Request erstellen wenn Ticket fertig abgearbeitet

* Pull Request wird gemerged nach 3x approve oder 48h nach Erstellung

Und einige Pro-Tipps ;) 

* Commite nur funktionstüchtigen Code

* Halte deine Commits atomar. Also commite Sinnesabschnitte.

* Nutze *rebase* statt *merge* um den neusten Master in deinen Branch zu bekommen.


### Nützliche Links ###

* Thymeleaf

    * [Tutorial: Using Thymeleaf](http://www.thymeleaf.org/doc/tutorials/2.1/usingthymeleaf.html)

    * [Tutorial: Thymeleaf + Spring](http://www.thymeleaf.org/doc/tutorials/2.1/thymeleafspring.html)

* [Bootstrap](http://getbootstrap.com/)


### Tipps und Tricks ###

* Intellij jammert wegen Spring-Konfiguration

    * Strg + Alt + Shift + S -> Modules -> Alte XML kram rausschmeißen -> "+"  -> Files Adden


### Tutorials ###

[Ein gutes Video-Tutorial](https://www.youtube.com/watch?v=GB8k2-Egfv0&list=PLC97BDEFDCDD169D7) zu Spring und der Datenbankanbindung mittels Hibernate. - Markus

[Ein Tutorial](http://docs.spring.io/spring-security/site/docs/3.2.x/guides/form.html) zum erstellen eines Custom Login Froms mit Controller und View über HTTPS.
Das Tutorials ist für eine ältere Version, also nicht mehr ganz zu gebrauchen. Ich lass es aber mal drin bis ich was besseres gefunden habe - Sebastian

[Tutorial für User Übersicht, Erstellen, Bearbeiten](http://www.mkyong.com/spring-mvc/spring-mvc-form-handling-example/) - Sebastian