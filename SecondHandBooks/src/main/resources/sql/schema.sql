-- MySQL dump 10.13  Distrib 5.6.27, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: _s0534006__sdf_dev
-- ------------------------------------------------------
-- Server version	5.6.27-2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `author` varchar(255) DEFAULT NULL,
  `bought` bit(1) DEFAULT NULL,
  `description` longtext,
  `highest_offer` double DEFAULT NULL,
  `image` longblob,
  `isbn` longtext,
  `payment_id` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `shipping_cost` double DEFAULT NULL,
  `showcase_datetime` datetime DEFAULT NULL,
  `title` longtext,
  `to_sale` bit(1) DEFAULT NULL,
  `highest_offer_user_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_hu21irv8vv89gdx0tilkp4ioo` (`highest_offer_user_id`),
  KEY `FK_4yrscb7n04n1865n7pnynlx1f` (`user_id`),
  CONSTRAINT `FK_4yrscb7n04n1865n7pnynlx1f` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_hu21irv8vv89gdx0tilkp4ioo` FOREIGN KEY (`highest_offer_user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `change_requests`
--

DROP TABLE IF EXISTS `change_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `change_requests` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deletionRequested` bit(1) DEFAULT NULL,
  `email` longtext,
  `name` varchar(250) DEFAULT NULL,
  `nickname` varchar(250) DEFAULT NULL,
  `payPalEmail` longtext,
  `surname` varchar(250) DEFAULT NULL,
  `notification_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_977l57c7ulycf09yhss0kf2kq` (`notification_id`),
  KEY `FK_s0e0grfhxjgesydfy81f5n1fa` (`user_id`),
  CONSTRAINT `FK_977l57c7ulycf09yhss0kf2kq` FOREIGN KEY (`notification_id`) REFERENCES `notifications` (`id`),
  CONSTRAINT `FK_s0e0grfhxjgesydfy81f5n1fa` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification_receiver`
--

DROP TABLE IF EXISTS `notification_receiver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_receiver` (
  `notification_id` bigint(20) NOT NULL,
  `receiver_id` bigint(20) NOT NULL,
  KEY `FK_3slfrgpvmpj6trvklsertbmxn` (`receiver_id`),
  KEY `FK_3lem0u5xxb8jfokvg5etblleh` (`notification_id`),
  CONSTRAINT `FK_3lem0u5xxb8jfokvg5etblleh` FOREIGN KEY (`notification_id`) REFERENCES `notifications` (`id`),
  CONSTRAINT `FK_3slfrgpvmpj6trvklsertbmxn` FOREIGN KEY (`receiver_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) DEFAULT NULL,
  `readed` bit(1) DEFAULT NULL,
  `time_added` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sender_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_8flb4cmdsv545bmcac8vx8e0a` (`sender_id`),
  CONSTRAINT `FK_8flb4cmdsv545bmcac8vx8e0a` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paypal_payments`
--

DROP TABLE IF EXISTS `paypal_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paypal_payments` (
  `id` varchar(255) NOT NULL,
  `approved` bit(1) DEFAULT NULL,
  `canceled` bit(1) DEFAULT NULL,
  `pay_key` varchar(20) DEFAULT NULL,
  `book_id` bigint(20) DEFAULT NULL,
  `payee_id` bigint(20) DEFAULT NULL,
  `payer_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_8uij62pj0cy3hvva5esplgwwn` (`book_id`),
  KEY `FK_fanmgdephyrbdg2o9s9aehbuv` (`payee_id`),
  KEY `FK_h0gxpi4pguu0xrlcsm2j9d7rp` (`payer_id`),
  CONSTRAINT `FK_8uij62pj0cy3hvva5esplgwwn` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`),
  CONSTRAINT `FK_fanmgdephyrbdg2o9s9aehbuv` FOREIGN KEY (`payee_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_h0gxpi4pguu0xrlcsm2j9d7rp` FOREIGN KEY (`payer_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `rating` float DEFAULT NULL,
  `rater_id` bigint(20) DEFAULT NULL,
  `seller_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_2uvaa78agwwkc81r5y5c9g3do` (`rater_id`),
  KEY `FK_dfkope067x7hjqgkq538wa7ml` (`seller_id`),
  CONSTRAINT `FK_2uvaa78agwwkc81r5y5c9g3do` FOREIGN KEY (`rater_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_dfkope067x7hjqgkq538wa7ml` FOREIGN KEY (`seller_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_g50w4r0ru3g9uf6i6fr4kpro8` (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` longtext NOT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `latest_showcase_datetime` datetime DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `nickname` varchar(250) DEFAULT NULL,
  `notifications_via_email` bit(1) DEFAULT NULL,
  `password` varchar(60) NOT NULL,
  `paypal_email` longtext,
  `rating` int(11) DEFAULT NULL,
  `showcase_locked` bit(1) DEFAULT NULL,
  `surname` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_2ty1xmrrgtn89xt7kyxx6ta7h` (`nickname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_roles`
--

DROP TABLE IF EXISTS `users_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_roles` (
  `user_id` bigint(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FK_k2mq1ee4ob6uw649wgaus1ate` (`role_id`),
  CONSTRAINT `FK_1hjw31qvltj7v3wb5o31jsrd8` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_k2mq1ee4ob6uw649wgaus1ate` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `verification_tokens`
--

DROP TABLE IF EXISTS `verification_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verification_tokens` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `expiry_date` datetime DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_dqp95ggn6gvm865km5muba2o5` (`user_id`),
  CONSTRAINT `FK_dqp95ggn6gvm865km5muba2o5` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-17 23:30:39
