package de.htw.secondhandbooks.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "change_requests")
public class ChangeRequest {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false)
	private Long id;
    
	@Column(length = 320)
	private String email;

	@Column(length = 320)
	private String payPalEmail;

	@Column(length = 250)
	private String name;

	@Column(length = 250)
	private String surname;

	@Column(length = 250)
	private String nickname;

    @Column
	private Boolean deletionRequested;

    @ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User sender;

    @OneToOne
	@JoinColumn(name = "notification_id")
	private Notification notification;

	public ChangeRequest() {
		this.deletionRequested = false;
	}

	public ChangeRequest(String name, String surname, String nickname, String email, String payPalEmail, User sender) {
		this.name = name;
		this.surname = surname;
		this.setNickname(nickname);
		this.email = email;
		this.payPalEmail = payPalEmail;
        this.sender = sender;
		this.deletionRequested = false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		if (nickname == null || nickname.isEmpty()) {
			this.nickname = null;
		} else {
			this.nickname = nickname;
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPayPalEmail() {
		return payPalEmail;
	}

	public void setPayPalEmail(String payPalEmail) {
		this.payPalEmail = payPalEmail;
	}

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public void setAsDeletionRequest() {
        this.deletionRequested = true;
    }

    public Boolean isDeletionRequest() {
        return this.deletionRequested;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

}
