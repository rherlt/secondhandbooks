package de.htw.secondhandbooks.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "paypal_payments")
public class PayPalPayment {
    @Id
    @Column(updatable = false)
    private String id;

    @Column(name = "pay_key", length = 20)
    private String payKey;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "payee_id", referencedColumnName = "id")
    private User payee;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "book_id", referencedColumnName = "id")
    private Book book;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "payer_id", referencedColumnName = "id")
    private User payer;

    @Column
    private boolean canceled;

    @Column
    private boolean approved;

    public PayPalPayment() {
    }

    public PayPalPayment(String id, User payee, Book book, User payer) {
        this.id = id;
        this.payee = payee;
        this.book = book;
        this.payer = payer;
        this.canceled = false;
        this.approved = false;
    }

    public PayPalPayment(String id, String payKey, User payee, Book book, User payer) {
        this.id = id;
        this.payKey = payKey;
        this.payee = payee;
        this.book = book;
        this.payer = payer;
        this.canceled = false;
        this.approved = false;
    }

    public void update(PayPalPayment changedPayment) {
        // TODO Change more data if needed.
        this.payKey = changedPayment.payKey;
        this.canceled = changedPayment.getCanceled();
        this.approved = changedPayment.getApproved();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPayKey() {
        return payKey;
    }

    public void setPayKey(String payKey) {
        this.payKey = payKey;
    }

    public User getPayee() {
        return payee;
    }

    public void setPayee(User payee) {
        this.payee = payee;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getPayer() {
        return payer;
    }

    public void setPayer(User payer) {
        this.payer = payer;
    }

    public boolean getCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public boolean getApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
}
