package de.htw.secondhandbooks.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "notifications")
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sender_id")
    private User sender;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "notification_receiver",
            joinColumns = { @JoinColumn(name = "notification_id")},
            inverseJoinColumns = {@JoinColumn(name = "receiver_id") })
    private List<User> receivers;

    @Column
    private String title;

    @Type(type = "text")
    @Column
    private String message;

    @Column(name = "time_added")
    private Date timeAdded;

    // Calling the variable read causes trouble
    @Column(name = "readed")
    private boolean read = false;

    public Notification(User sender, User receiver, String title, String message) {
        this(sender, new ArrayList<User>(), title, message);

        this.receivers.add(receiver);
    }

    public Notification(User sender, List<User> receivers, String title, String message) {
        this.sender = sender;
        this.receivers = receivers;
        this.message = message;
        this.read = false;
        this.timeAdded = Calendar.getInstance().getTime();
        this.title = title;
    }

    public Notification() {
        this.read = false;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public List<User> getReceivers() {
        return receivers;
    }

    public void setReceivers(List<User> receivers) {
        this.receivers = receivers;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle(){
        return this.title;
    }

    public Date getTimeAdded() {
        return this.timeAdded;
    }
    
    @Override
    public String toString() {
        return "Notification{" +
                "id=" + id +
                ", sender=" + sender.getId() +
                ", message=" + message +
                ", read=" + read +
                '}';
    }
}
