package de.htw.secondhandbooks.model;

import org.hibernate.annotations.Type;

import javax.imageio.ImageIO;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Date;

@Entity
@Table(name = "books")
public class Book {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(updatable = false)
	private Long id;

	@Column(nullable = true, length = 500)
	private String title;

	@Column(nullable = true)
	private String author;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(nullable = true)
	private byte[] image;

	@Column
	private boolean bought;

	@Type(type = "text")
	@Column(nullable = true)
	private String description;

	@Column(nullable = true, length = 320)
	private String isbn;

	@Column(name = "showcase_datetime", nullable = true)
	private Date showcaseDateTime;

	@Column(name = "to_sale")
	private boolean toSale;

	@Column
	private double price;

	@Column(name = "shipping_cost")
	private double shippingCost;

	@Column(name = "highest_offer")
	private double highestOffer;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "highest_offer_user_id", referencedColumnName = "id")
	private User highestOfferUser;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User owner;

	@Column(name = "payment_id")
	private String paymentId;

	public Book() {

	}

	public Book(String title, byte[] image, User owner, String isbn, String author) {
		this.title = title;
		this.image = image;
		this.bought = false;
		this.toSale = false;
		this.owner = owner;
		this.price = 0;
		this.shippingCost = 0;
		this.highestOffer = 0;
		this.highestOfferUser = null;
		this.isbn = isbn;
		this.author = author;
		this.paymentId = null;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the image
	 */
	public byte[] getImage() {
		return image;
	}
	
	/**
	 * @return the image
	 */
	public String getImageBase64() {
		if (image == null) {
			return null;
		}
		return "data:image/jpg;base64," + Base64.getEncoder().encodeToString(image);
	}

	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(BufferedImage image) {
		if (image == null) {			
			this.image = null;
		} else {			
			Image scaledImage = image.getScaledInstance(333, 500, Image.SCALE_SMOOTH);
			BufferedImage scaledBufferedImage = new BufferedImage(scaledImage.getWidth(null), scaledImage.getHeight(null),
					BufferedImage.TYPE_INT_RGB);
			scaledBufferedImage.getGraphics().drawImage(scaledImage, 0, 0, null);
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			try {
				ImageIO.write(scaledBufferedImage, "jpeg", output);
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.image = output.toByteArray();
		}
	}

	/**
	 * @return the bought
	 */
	public boolean isBought() {
		return bought;
	}

	/**
	 * @param bought
	 *            the bought to set
	 */
	public void setBought(boolean bought) {
		this.bought = bought;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the toSale
	 */
	public boolean isToSale() {
		return toSale;
	}

	/**
	 * @param toSale
	 *            the toSale to set
	 */
	public void setToSale(boolean toSale) {
		this.toSale = toSale;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getShippingCost() {
		return shippingCost;
	}

	public void setShippingCost(double shippingCost) {
		this.shippingCost = shippingCost;
	}

	public double getOverallCost() {
		if (isBought()) {			
			return getHighestOffer() + getShippingCost();
		}
		return getPrice() + getShippingCost();
	}

    public String getPriceText() {
        return String.format("%.2f €", this.getPrice());
    }

    public String getShippingCostText() {
        return String.format("%.2f €", this.getShippingCost());
    }

    public String getOverallCostText() {
        return String.format("%.2f €", this.getOverallCost());
    }

    public String getHighestOfferText() {
        return String.format("%.2f €", this.getHighestOffer());
    }

	public Date getShowcaseDateTime() {
		return showcaseDateTime;
	}

	public void setShowcaseDateTime(Date showcaseDateTime) {
		this.showcaseDateTime = showcaseDateTime;
	}

	public void update(Book changedBook) {
		this.title = changedBook.getTitle();
		this.image = changedBook.getImage();
		this.toSale = changedBook.isToSale();
		this.description = changedBook.getDescription();
		this.bought = changedBook.isBought();
		this.owner = changedBook.getOwner();
		this.price = changedBook.getPrice();
		this.shippingCost = changedBook.getShippingCost();
		this.highestOffer = changedBook.getHighestOffer();
		this.highestOfferUser = changedBook.getHighestOfferUser();
		this.isbn = changedBook.getIsbn();
		this.author = changedBook.getAuthor();
		this.showcaseDateTime = changedBook.getShowcaseDateTime();
		this.paymentId = changedBook.getPaymentId();
	}

	public String toString() {
		boolean hasImage = false;
		Long highestOfferUserId = Long.valueOf(0);

		if (this.image != null)
			hasImage = true;

		if (this.getHighestOfferUser() != null)
			highestOfferUserId = this.getHighestOfferUser().getId();

		return "Title - " + this.title + System.lineSeparator() + "Description - " + this.description
				+ System.lineSeparator() + "toSale - " + this.isToSale() + System.lineSeparator() + "bought - "
				+ this.isBought() + System.lineSeparator() + "owner - " + this.owner.getId() + System.lineSeparator()
				+ "Price - " + this.getPrice() + System.lineSeparator() + "Shipping cost - " + this.getShippingCost()
				+ System.lineSeparator() + "Image - " + hasImage + System.lineSeparator() + "HighestOffer - "
				+ this.getHighestOffer() + System.lineSeparator() + "HighestOfferUserId - " + highestOfferUserId
				+ System.lineSeparator() + "PaymentId - " + this.paymentId + System.lineSeparator();
	}
	
	public double getHighestOffer() {
		return highestOffer;
	}

	public void setHighestOffer(double highestOffer) {
		this.highestOffer = highestOffer;
	}

	public User getHighestOfferUser() {
		return highestOfferUser;
	}

	public void setHighestOfferUser(User highestOfferUser) {
		this.highestOfferUser = highestOfferUser;
	}

    public boolean hasOffer() {
        return this.highestOffer > 0 && this.highestOfferUser != null;
    }

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
}
