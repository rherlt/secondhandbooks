package de.htw.secondhandbooks.model;

import de.htw.secondhandbooks.dto.UserChangeRequestDTO;
import de.htw.secondhandbooks.dto.UserDTO;

import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false)
	private Long id;

	@Column(unique = true, nullable = false, length = 320)
	private String email;

	@Column(name = "paypal_email", unique = true, length = 320)
	private  String payPalEmail;

	@Column(nullable = false, length = 60)
	private String password;

	@Column(nullable = false, length = 250)
	private String name;

	@Column(nullable = false, length = 250)
	private String surname;

	@Column(nullable = true, length = 250, unique = true)
	private String nickname;

	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE })
	@JoinTable(name = "users_roles", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
			@JoinColumn(name = "role_id") })
	private Set<Role> roles = new HashSet<Role>();

	@OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.REMOVE }, mappedBy = "owner")
	private List<Book> books = new ArrayList<>();

	@OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE }, mappedBy = "highestOfferUser")
	private List<Book> offers = new ArrayList<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.REMOVE }, mappedBy = "sender")
    private List<ChangeRequest> changeRequests = new ArrayList<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.REMOVE }, mappedBy = "sender")
	private List<Notification> sentNotifications = new ArrayList<>();

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.REMOVE }, mappedBy = "receivers")
    private Set<Notification> receivedNotifications = new HashSet<>();
    
    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.REMOVE }, mappedBy = "payee")
    private List<PayPalPayment> receivedPayPalPayments = new ArrayList<>();
    
    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.REMOVE }, mappedBy = "payer")
    private List<PayPalPayment> sentPayPalPayments = new ArrayList<>();

	@Column
	private Boolean enabled;

	@Column(name = "showcase_locked")
	private Boolean showcaseLocked;

	@Column
	private Integer rating;

	@Column(name = "notifications_via_email")
	private Boolean notificationsViaEmail;

	@Column(name = "latest_showcase_datetime", nullable = true)
	private Date latestShowcaseDateTime;

	public User() {
	}

	public User(String name, String surname, String nickname, String email, String payPalEmail, String password) {
		this.name = name;
		this.surname = surname;
		this.setNickname(nickname);
		this.email = email;
		this.payPalEmail = payPalEmail;
		this.password = password;
		this.enabled = false;
		this.showcaseLocked = false;
		this.notificationsViaEmail = false;
		this.latestShowcaseDateTime = null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		if (nickname == null || nickname.isEmpty()) {
			this.nickname = null;
		} else {
			this.nickname = nickname;
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPayPalEmail() {
		return payPalEmail;
	}

	public void setPayPalEmail(String payPalEmail) {
		this.payPalEmail = payPalEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getShowcaseLocked() {
		return showcaseLocked;
	}

	public void setShowcaseLocked(Boolean showcaseLocked) {
		this.showcaseLocked = showcaseLocked;
	}

	public Boolean getNotificationsViaEmail() {
		return notificationsViaEmail;
	}

	public void setNotificationsViaEmail(Boolean notificationsViaEmail) {
		this.notificationsViaEmail = notificationsViaEmail;
	}

	public Date getLatestShowcaseDateTime() {
		return latestShowcaseDateTime;
	}

	public void setLatestShowcaseDateTime(Date latestShowcaseDateTime) {
		this.latestShowcaseDateTime = latestShowcaseDateTime;
	}

	public List<Book> getBooks() {
		return new ArrayList<Book>(books);
	}

	public void addBooks(Collection<Book> books) {
		for (Book book : books) {
			this.books.add(book);
			book.setOwner(this);
		}
	}

	public void removeBook(Book book) {
		if (getBooks().contains(book)) {
			getBooks().remove(book);
			book.setOwner(null);
		}
	}

	public List<Book> getOffers() {
		return offers;
	}

    public Set<Notification> getReceivedNotifications() {
        return receivedNotifications;
    }

	public void addOffers(Collection<Book> books) {
		for (Book book : books) {
			this.offers.add(book);
			book.setHighestOfferUser(this);
		}
	}

	public void addOffer(Book book) {
		this.offers.add(book);
		book.setHighestOfferUser(this);
	}

	public void removeOffer(Book book) {
		if (getOffers().contains(book)) {
			getOffers().remove(book);
			book.setHighestOfferUser(null);
		}
	}

	// TODO: deprecated? Used in view and UserService
	public List<Book> getBooksOnSale() {
		ArrayList<Book> bookArrayList = (ArrayList<Book>) getBooks();
		ArrayList<Book> bookOnSaleArrayList = new ArrayList<Book>();
		for (int i = 0; i < bookArrayList.size(); i++) {
			Book book = bookArrayList.get(i);
			if (book.isToSale() && !book.isBought())
				bookOnSaleArrayList.add(book);
		}

		// quick hack to sort by showcasedatetime
		bookOnSaleArrayList.sort((b1, b2) -> b2.getShowcaseDateTime().compareTo(b1.getShowcaseDateTime()));

		return bookOnSaleArrayList;
	}

	/**
	 * Returns -1 if not rated yet.
	 * @return
     */
	public Integer getRating() {
		Integer rating = this.rating;
		if (rating == null) {
			rating = -1;
		}
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
	
    /**
     * This method will return either the nickname or if no nickname is available name + surname.
     *
     * @return Nickname or Full name
     */
    public String getDisplayName() {
        String name = null;

        if (this.nickname != null) {
            name = this.nickname;
        }
        else {
            name = this.name + " " + this.surname;
        }

        return name;
    }

	public void update(User changedUser) {
		this.email = changedUser.getEmail();
		this.payPalEmail = changedUser.getPayPalEmail();
		this.password = changedUser.getPassword();
		this.name = changedUser.getName();
		this.surname = changedUser.getSurname();
		this.roles = changedUser.getRoles();
		this.enabled = changedUser.getEnabled();
		this.books = changedUser.getBooks();
		this.offers = changedUser.getOffers();
		this.nickname = changedUser.getNickname();
		this.showcaseLocked = changedUser.getShowcaseLocked();
		this.notificationsViaEmail = changedUser.getNotificationsViaEmail();
		this.latestShowcaseDateTime = changedUser.getLatestShowcaseDateTime();
		this.rating = changedUser.getRating();
	}

	public void update(UserDTO userDTO) {
		this.setName(userDTO.getName());
		this.setSurname(userDTO.getSurname());
		this.setEmail(userDTO.getEmail());
		this.setPayPalEmail(userDTO.getPayPalEmail());
		this.setNickname(userDTO.getNickname());
		if (userDTO.getRoles() != null) {
			this.roles = userDTO.getRoles();
		}
		this.setShowcaseLocked(userDTO.isShowcaseLocked());
		this.setNotificationsViaEmail(userDTO.isNotificationsViaEmail());
	}

	public void update(UserChangeRequestDTO userChangeRequestDTO) {
		this.setName(userChangeRequestDTO.getName());
		this.setSurname(userChangeRequestDTO.getSurname());
		this.setNickname(userChangeRequestDTO.getNickname());
		this.setEmail(userChangeRequestDTO.getEmail());
		this.setPayPalEmail(userChangeRequestDTO.getPayPalEmail());
	}

	public String toString() {
		String books = "";
		if (this.getBooks() != null) {
			for (int i = 0; i < this.getBooks().size(); i++) {
				books += System.lineSeparator() + "Book #" + i + " - " + this.getBooks().get(i).getId();
			}
		}

		String offers = "";
		if (this.getOffers() != null) {
			for (int i = 0; i < this.getOffers().size(); i++) {
				offers += System.lineSeparator() + "Offer #" + i + " - " + this.getOffers().get(i).getId();
			}
		}

		return "ID - " + this.id  + System.lineSeparator()
				+ "Name - " + this.name + System.lineSeparator()
				+ "Nickname - " + this.nickname + System.lineSeparator()
				+ "Surname - " + this.surname + System.lineSeparator()
				+ "Email - " + this.email + System.lineSeparator()
				+ "PayPal email - " + this.payPalEmail + System.lineSeparator()
				+ "Password - " + this.password + System.lineSeparator()
				+ "Enabled - " + this.enabled + System.lineSeparator()
				+ "ShowcaseLocked - " + this.showcaseLocked + System.lineSeparator()
				+ "NotificationsViaEmail - " + this.notificationsViaEmail + System.lineSeparator()
				+ "LatestShowcaseDateTime - " + this.latestShowcaseDateTime + System.lineSeparator()
				+ "Rating - " + this.rating + System.lineSeparator()
				+ books + offers;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof User)) return false;

		User user = (User) o;

		if (getId() != null ? !getId().equals(user.getId()) : user.getId() != null) return false;
		if (getEmail() != null ? !getEmail().equals(user.getEmail()) : user.getEmail() != null) return false;
		if (getPayPalEmail() != null ? !getPayPalEmail().equals(user.getPayPalEmail()) : user.getPayPalEmail() != null)
			return false;
		if (getPassword() != null ? !getPassword().equals(user.getPassword()) : user.getPassword() != null)
			return false;
		if (getName() != null ? !getName().equals(user.getName()) : user.getName() != null) return false;
		if (getSurname() != null ? !getSurname().equals(user.getSurname()) : user.getSurname() != null) return false;
		if (getNickname() != null ? !getNickname().equals(user.getNickname()) : user.getNickname() != null)
			return false;
		if (getRoles() != null ? !getRoles().equals(user.getRoles()) : user.getRoles() != null) return false;
		if (getBooks() != null ? !getBooks().equals(user.getBooks()) : user.getBooks() != null) return false;
		if (getOffers() != null ? !getOffers().equals(user.getOffers()) : user.getOffers() != null) return false;
		if (getShowcaseLocked() != null ? !getShowcaseLocked().equals(user.getShowcaseLocked()) : user.getShowcaseLocked() != null) return false;
		if (getNotificationsViaEmail() != null ? !getNotificationsViaEmail().equals(user.getNotificationsViaEmail()) : user.getNotificationsViaEmail() != null) return false;
		if (getLatestShowcaseDateTime() != null ? !getLatestShowcaseDateTime().equals(user.getLatestShowcaseDateTime()) : user.getLatestShowcaseDateTime() != null) return false;
		if (getRating() != null ? !getRating().equals(user.getRating()) : user.getRating() != null) return false;
		return !(getEnabled() != null ? !getEnabled().equals(user.getEnabled()) : user.getEnabled() != null);
	}

	@Override
	public int hashCode() {
		int result = getId() != null ? getId().hashCode() : 0;
		result = 31 * result + (getEmail() != null ? getEmail().hashCode() : 0);
		result = 31 * result + (getPayPalEmail() != null ? getPayPalEmail().hashCode() : 0);
		result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
		result = 31 * result + (getName() != null ? getName().hashCode() : 0);
		result = 31 * result + (getSurname() != null ? getSurname().hashCode() : 0);
		result = 31 * result + (getNickname() != null ? getNickname().hashCode() : 0);
		result = 31 * result + (getRoles() != null ? getRoles().hashCode() : 0);
		result = 31 * result + (getBooks() != null ? getBooks().hashCode() : 0);
		result = 31 * result + (getOffers() != null ? getOffers().hashCode() : 0);
		result = 31 * result + (getEnabled() != null ? getEnabled().hashCode() : 0);
		result = 31 * result + (getShowcaseLocked() != null ? getShowcaseLocked().hashCode() : 0);
		result = 31 * result + (getNotificationsViaEmail() != null ? getNotificationsViaEmail().hashCode() : 0);
		result = 31 * result + (getLatestShowcaseDateTime() != null ? getLatestShowcaseDateTime().hashCode() : 0);
		result = 31 * result + (getRating() != null ? getRating().hashCode() : 0);
		return result;
	}
}
