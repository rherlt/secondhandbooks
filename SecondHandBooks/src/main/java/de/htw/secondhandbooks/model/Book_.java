package de.htw.secondhandbooks.model;

import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.User;

import javax.persistence.metamodel.SingularAttribute;

@javax.persistence.metamodel.StaticMetamodel(de.htw.secondhandbooks.model.Book.class)
public class Book_ {
    public static volatile SingularAttribute<Book, Long> id;
    public static volatile SingularAttribute<Book, User> owner;
}
