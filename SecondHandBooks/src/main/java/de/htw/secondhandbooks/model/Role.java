package de.htw.secondhandbooks.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
 
@Entity
@Table(name = "roles")
public class Role {
 
    @Id 
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id; 
 
    @Column(length=15, unique=true, nullable=false)
    private String role = RoleType.USER.getRoleType();

    public int getId() {
        return id;
    }
 
    public void setId(int id) {
        this.id = id;
    }
 
    public String getRole() {
        return role;
    }
 
    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Role [id=" + id + ",  role=" + role  + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Role)) return false;

        Role role = (Role) o;

        if (getId() != role.getId()) return false;
        return getRole().equals(role.getRole());

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getRole().hashCode();
        return result;
    }
}
