package de.htw.secondhandbooks.model;

public enum RoleType {
    USER("USER"),
    ADMIN("ADMIN");
     
    String roleType;
     
    private RoleType(String roleType){
        this.roleType = roleType;
    }
     
    public String getRoleType(){
        return roleType;
    }
     
}
