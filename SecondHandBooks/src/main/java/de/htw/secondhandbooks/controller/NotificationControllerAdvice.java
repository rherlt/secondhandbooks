package de.htw.secondhandbooks.controller;

import de.htw.secondhandbooks.model.Notification;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.service.NotificationService;
import de.htw.secondhandbooks.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;

@ControllerAdvice
public class NotificationControllerAdvice {

	@Autowired
	NotificationService notificationService;
	@Autowired
	UserService userService;

	@ModelAttribute
	public void notificationNumber(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String email = auth.getName();
		if (email != null && email != "") {
			User user = userService.findUserByEmail(email);
			List<Notification> notifications = notificationService.getAllUnreadNotificationsForUser(user);
			model.addAttribute("notificationNumber", notifications.size());
		}
	}

}
