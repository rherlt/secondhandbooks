package de.htw.secondhandbooks.controller;

import de.htw.secondhandbooks.dto.RatingDTO;
import de.htw.secondhandbooks.model.Rating;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.service.BookService;
import de.htw.secondhandbooks.service.RatingService;
import de.htw.secondhandbooks.service.UserService;
import de.htw.secondhandbooks.validation.RateFormValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.sql.Date;
import java.time.Instant;

@Controller
public class RatingController {

	@Autowired
	BookService bookService;
	@Autowired
	UserService userService;
	@Autowired
    RatingService ratingService;
	@Autowired
    RateFormValidation rateFormValidation;

    @InitBinder("ratingDTO")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(rateFormValidation);
    }

	@RequestMapping(value = "/rate/{seller_id}", method = RequestMethod.GET)
    public String showRatingForm( @PathVariable("seller_id") Long sellerId,
                                 Model model, Principal principal) {

		User rater = userService.findUserByEmail(principal.getName());
		User seller = userService.findUserById(sellerId);

        Rating rating = ratingService.findOneByRaterAndSeller(rater,seller);

        RatingDTO ratingDTO =  new RatingDTO();

        if (rating == null) {
            return "redirect:/access-denied";
        }

        boolean newRating = true;
        if (rating.getRating() >= 0 ) {
            newRating = false;
        }

        ratingDTO.setRating(rating.getRating());
        if (rating.getComment() == null) {
            ratingDTO.setComment("");
        } else {
            ratingDTO.setComment(rating.getComment());
        }

		model.addAttribute("rater", rater);
		model.addAttribute("newRating", newRating);
		model.addAttribute("seller", seller);
		model.addAttribute("ratingDTO", ratingDTO);

		return "rate-seller";
	}

    @RequestMapping(value = "/rate/{seller_id}", method = RequestMethod.POST)
    public ModelAndView saveRating(@PathVariable("seller_id") long sellerId,
                                   @ModelAttribute("ratingDTO") @Validated RatingDTO ratingDTO,
                                   BindingResult result,
                                   Model model, Principal principal) {

        if (result.hasErrors()) {
            User seller = userService.findUserById(sellerId);
            model.addAttribute("seller", seller);
            return new ModelAndView("rate-seller", "BindingAwareModelMap", model);
        }

        User rater = userService.findUserByEmail(principal.getName());

        Rating rating;
        if (ratingDTO.getId() == null || ratingDTO.getId().isEmpty()) {
            rating = new Rating();
        } else {
            rating = ratingService.findRatingById(Long.valueOf(ratingDTO.getId()));
        }

        rating.setDate(Date.from(Instant.now()));
        rating.setRating(ratingDTO.getRating());
        rating.setComment(ratingDTO.getComment());
        rating.setSeller(userService.findUserById(sellerId));
        rating.setRater(rater);

        ratingService.createOrUpdateRating(rating);
        return new ModelAndView("redirect:/showcases", "BindingAwareModelMap", model);
	}
}
