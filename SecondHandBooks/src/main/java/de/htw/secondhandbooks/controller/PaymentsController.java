package de.htw.secondhandbooks.controller;

import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.Notification;
import de.htw.secondhandbooks.model.PayPalPayment;
import de.htw.secondhandbooks.model.Rating;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.payments.PayPalPayments;
import de.htw.secondhandbooks.service.BookService;
import de.htw.secondhandbooks.service.NotificationService;
import de.htw.secondhandbooks.service.PayPalPaymentService;
import de.htw.secondhandbooks.service.RatingService;
import de.htw.secondhandbooks.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Objects;

@Controller
public class PaymentsController {
    @Autowired
    UserService userService;
    @Autowired
    BookService bookService;
    @Autowired
    NotificationService notificationService;
    @Autowired
    RatingService ratingService;
    @Autowired
    PayPalPaymentService payPalPaymentService;

    @RequestMapping(value = "/pay-book/{paymentId}", method = RequestMethod.GET)
    public String payBook(@PathVariable("paymentId") String paymentId, Principal principal, HttpServletRequest request,
                          RedirectAttributes redirectAttributes) {
        PayPalPayment payment = payPalPaymentService.findPaymentById(paymentId);
        Book book = payment.getBook();
        User user = userService.findUserByEmail(principal.getName());

        if (book == null || user == null)
            return "redirect:/offersoverview";
        if (!book.isToSale() || !book.getHighestOfferUser().getId().equals(user.getId()) || !book.isBought())
            return "redirect:/offersoverview";

        String payKey = payment.getPayKey();

        if (payKey == null) {
            payKey = PayPalPayments.createPayment(book, request);

            if (payKey == null) {
                redirectAttributes.addFlashAttribute("messageText", "There was an error during payment. Try again later.");
                redirectAttributes.addFlashAttribute("messageDanger", true);
                return "redirect:/";
            }

            payment.setPayKey(payKey);
            payPalPaymentService.updatePayment(payment);
        } else if (PayPalPayments.isPaymentSuccessfull(payKey)) {
            return returnUrl(paymentId, principal, request, redirectAttributes);
        }

        return "redirect:" + PayPalPayments.getApproveUrl(payKey);
    }

    @RequestMapping(value = "/cancelled-payment", method = RequestMethod.GET)
    public String cancelUrl(@RequestParam("id") String id, Principal principal) {
        PayPalPayment payment = payPalPaymentService.findPaymentById(id);

        User user = userService.findUserByEmail(principal.getName());

        if (payment == null || payment.getCanceled() || payment.getApproved() ||
                !Objects.equals(user.getId(), payment.getPayer().getId())) {
            return "redirect:/offersoverview";
        }

        Book book = payment.getBook();

        payment.setCanceled(true);
        payPalPaymentService.updatePayment(payment);

        return "redirect:/cancel-offer/" + book.getId();
    }

    @RequestMapping(value = "/approved-payment", method = RequestMethod.GET)
    public String returnUrl(@RequestParam("id") String id, Principal principal, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        PayPalPayment payment = payPalPaymentService.findPaymentById(id);

        User user = userService.findUserByEmail(principal.getName());

        if (payment == null || payment.getCanceled() || payment.getApproved() ||
                !Objects.equals(user.getId(), payment.getPayer().getId())) {
        	redirectAttributes.addFlashAttribute("messageText", "There was an error during payment. Try again later.");
        	redirectAttributes.addFlashAttribute("messageDanger", true);
            return "redirect:/buy-book/" + id;
        }

        payment.setApproved(true);
        payPalPaymentService.updatePayment(payment);

        Book book = payment.getBook();
        ratingRequest(book, user, request);
        changeBookOwner(book, user, request);

        redirectAttributes.addFlashAttribute("messageText", "You have successfully bought " + book.getTitle() + " from "
				+ payment.getPayee().getDisplayName());
        redirectAttributes.addFlashAttribute("messageSuccess", true);

        return "redirect:/bookoverview";
    }

    private void ratingRequest(Book book, User user, HttpServletRequest request) {
        User seller = book.getOwner();
        Rating rating = ratingService.findOneByRaterAndSeller(user, seller);
        if (rating == null || rating.getRating() == -1) {
            Rating newRating = new Rating();
            newRating.setRater(user);
            newRating.setSeller(seller);
            newRating.setRating(-1);
            ratingService.createOrUpdateRating(newRating);

            Notification notification = notificationService.createNotificationForRatingRequest(seller, user, request);
            notificationService.persistNotification(notification);
        }
        else {
            Notification notification = notificationService.createNotificationForRatingUpdateRequest(seller, user,
                    request);
            notificationService.persistNotification(notification);
        }
    }

    private void changeBookOwner(Book book, User newUser, HttpServletRequest request) {
        User oldUser = book.getOwner();
        oldUser.removeBook(book);
        book.setHighestOffer(0);
        book.setHighestOfferUser(null);
        book.setToSale(false);
        book.setBought(false);
        book.setPrice(0);
        book.setShippingCost(0);
        book.setOwner(newUser);
        book.setShowcaseDateTime(null);
        newUser.getOffers().remove(book);
        newUser.getBooks().add(book);
        userService.updateUser(oldUser);
        userService.updateUser(newUser);
        bookService.updateBook(book);
        
        Notification notification = notificationService.createNotificationForBookReceived(oldUser, newUser, book, request);
		notificationService.persistNotification(notification);
    }
}
