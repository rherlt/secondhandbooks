package de.htw.secondhandbooks.controller;

import de.htw.secondhandbooks.dto.PasswordResetDTO;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.service.UserService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class PasswordResetController {

	@Autowired
	UserService userService;

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@RequestMapping(value = "/forgot-password", method = RequestMethod.GET)
	public String showPasswordResetForm(Model model) {
		PasswordResetDTO passwordResetDTO = new PasswordResetDTO();
		model.addAttribute("passwordResetDTO", passwordResetDTO);
		return "forgot-password";
	}

	@RequestMapping(value = "/forgot-password", method = RequestMethod.POST)
	public String resetPassword(@ModelAttribute("PasswordResetDTO") @Valid PasswordResetDTO passwordResetDTO,
			BindingResult result, Model model, RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {			
			model.addAttribute("passwordResetDTO", passwordResetDTO);
			return "forgot-password";
		}

		User user = userService.findUserByEmail(passwordResetDTO.getEmail());
		if (user == null) {
			model.addAttribute("passwordResetDTO", passwordResetDTO);
			model.addAttribute("messageText", "No corresponding profile found");
			model.addAttribute("messageDanger", true);
			return "forgot-password";
		}

		if (user.getSurname().equals(passwordResetDTO.getSurname())
				&& user.getName().equals(passwordResetDTO.getName())) {
			String newPassword = RandomStringUtils.randomAlphanumeric(8);
			user.setPassword(passwordEncoder.encode(newPassword));
			userService.createOrUpdateUser(user);

			SimpleMailMessage email = constructEmailMessage(passwordResetDTO.getEmail(), newPassword);
			mailSender.send(email);
		} else {
			model.addAttribute("messageText", "No corresponding profile found");
			model.addAttribute("messageDanger", true);
			return "forgot-password";
		}

		redirectAttributes.addFlashAttribute("messageText", "A new password was send to you");
        redirectAttributes.addFlashAttribute("messageSuccess", true);
		return "redirect:/";
	}

	private SimpleMailMessage constructEmailMessage(String recipientAddress, String password) {
		final String subject = "New Password for SecondHandBooks";
		final String message = "Dear customer," + System.lineSeparator() + System.lineSeparator()
				+ "you have requested a new password via our website." + System.lineSeparator()
				+ "Your new password is: " + password + System.lineSeparator()
				+ "Please change this password in the near future to keep your account secure." + System.lineSeparator()
				+ System.lineSeparator() + "Your SecondHandBooks Support";

		final SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(recipientAddress);
		email.setSubject(subject);
		email.setText(message);

		return email;
	}
}
