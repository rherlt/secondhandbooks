package de.htw.secondhandbooks.controller;

import de.htw.secondhandbooks.config.PaginationPropertiesConfig;
import de.htw.secondhandbooks.dto.BookDTO;
import de.htw.secondhandbooks.filter.BookFilter;
import de.htw.secondhandbooks.filter.BookSortFilter;
import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.service.BookService;
import de.htw.secondhandbooks.service.UserService;
import de.htw.secondhandbooks.utils.Filters;
import de.htw.secondhandbooks.validation.BookFormValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.security.Principal;

@Controller
@SessionAttributes({"bookFilter", "bookSortFilter"})
public class BookController {

	private static final Logger logger = LoggerFactory.getLogger(BookController.class);

	@Autowired
	BookService bookService;
	@Autowired
	UserService userService;
	@Autowired
	BookFormValidation validator;
	@Autowired
	PaginationPropertiesConfig paginationPropertiesLoader;

	@ModelAttribute("bookFilter")
	public BookFilter initBookFilter() {
		return new BookFilter();
	}

	@ModelAttribute("bookSortFilter")
	public BookSortFilter initBookSortFilter() {
		return new BookSortFilter();
	}

	@InitBinder("bookDTO")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String editBookForm(@PathVariable("id") long id, Model model, Principal principal, BookDTO bookDTO) {
		User user = userService.findUserByEmail(principal.getName());
		Book book = bookService.findBookById(id);
		
		if (!book.getOwner().getId().equals(user.getId()))
			return "redirect:/access-denied";
		
		String base64Image = book.getImageBase64();
		if (base64Image != null) {			
			model.addAttribute("bookBase64Image", book.getImageBase64());
		}
		bookDTO.setBookData(book);
		model.addAttribute("bookDTO", bookDTO);
		return "edit-book";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addBookForm(Model model, Principal principal) {
		BookDTO bookDTO = new BookDTO();
		model.addAttribute("newBook", true);
		model.addAttribute("bookDTO", bookDTO);

		return "edit-book";
	}

	@RequestMapping(value = "/bookoverview", method = { RequestMethod.GET, RequestMethod.POST })
	public String bookOverviewPage(@ModelAttribute("bookFilter") BookFilter bookFilter,
								   @ModelAttribute("bookSortFilter") BookSortFilter bookSortFilter,
								   WebRequest webRequest,
								   Model model,
								   Principal principal) {
		return this.bookOverviewPagination(bookFilter, bookSortFilter, webRequest, 1, model, principal);
	}

	@RequestMapping(value = "/bookoverview/{page}", method = { RequestMethod.GET, RequestMethod.POST })
	public String bookOverviewPagination(@ModelAttribute("bookFilter") BookFilter bookFilter,
										 @ModelAttribute("bookSortFilter") BookSortFilter bookSortFilter,
										 WebRequest webRequest,
										 @PathVariable("page") Integer page,
										 Model model,
										 Principal principal) {
		if (webRequest.getParameter("search-term-filter") != null && webRequest.getParameter("search-term") != null) {
			logger.debug("search-term-filter: " + webRequest.getParameter("search-term-filter"));
			logger.debug("search-term:        " + webRequest.getParameter("search-term"));
		}

		if (webRequest.getParameter("on-sale-filter") != null) {
			logger.debug("on-sale-filter: " + webRequest.getParameter("on-sale-filter"));
		}


		bookFilter.setSearchPage("bookoverview");
		Filters.setBookFilter(bookFilter, webRequest);

		if (bookSortFilter.getSortPage() != null && !bookSortFilter.getSortPage().equals("bookoverview")) {
			Filters.resetSortFilter(bookSortFilter);
		}
		bookSortFilter.setSortPage("bookoverview");
		Filters.setBookSortFilter(bookSortFilter, webRequest);

		User user = userService.findUserByEmail(principal.getName());
		Page<Book> books = bookService.findAllByOwnerId(user.getId(), page, paginationPropertiesLoader.getPaginationItemsPerPage("pagination.items.bookoverview"), bookFilter, false, bookSortFilter);

		boolean allowSell = false;
		if (user.getPayPalEmail() != null && !user.getPayPalEmail().equals(""))
			allowSell = true;
		model.addAttribute("allowSell", allowSell);
		model.addAttribute("title", "Bookoverview");
		model.addAttribute("books", books.getContent());
		model.addAttribute("page", books);
		model.addAttribute("bookFilter", bookFilter);
		model.addAttribute("user", user);

		return "bookoverview";
	}
	
	@RequestMapping(value = "/update-book", method = RequestMethod.POST)
	public String updateBook(@ModelAttribute("bookDTO") @Validated BookDTO bookDTO, BindingResult result, Principal principal) {

		if (result.hasErrors()) {
			return "edit-book";
		} else {

			User owner = userService.findUserByEmail(principal.getName());
			bookDTO.setOwner(owner);

			// Get the already existing book or create a new one
			Book book;

			if (bookDTO.getId() == null || bookDTO.getId().isEmpty()) {
				book = new Book();
			} else if (!bookService.findBookById(Long.valueOf(bookDTO.getId())).getOwner().getId().equals(owner.getId())) {
				return "redirect:/access-denied";
			} else {
				book = bookService.findBookById(Long.valueOf(bookDTO.getId()));

			}

			book.setOwner(bookDTO.getOwner());
			book.setTitle(bookDTO.getTitle());
			book.setAuthor(bookDTO.getAuthor());
			book.setDescription(bookDTO.getDescription());
			book.setIsbn(bookDTO.getIsbn());

			// if file uploaded
			if (!(bookDTO.getImage().isEmpty() && !bookDTO.isRemoveImage())) {
				try {
					book.setImage(ImageIO.read(bookDTO.getImage().getInputStream()));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			if (bookDTO.isRemoveImage())
				book.setImage(null);

			// If this book existed we need to update, otherwise create a new one
			if (bookDTO.getId() == null || bookDTO.getId().isEmpty() || bookDTO.getId().equals("")) {
				//book = bookService.findBookById(Long.valueOf(bookDTO.getId()));
				bookService.createBook(book);
			} else {
				bookService.updateBook(book);

			}

			return "redirect:/bookoverview";
		}
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String BookDeletePage(@PathVariable("id") long id, Principal principal, RedirectAttributes redirectAttributes) {
		User owner = userService.findUserByEmail(principal.getName());
		Book book = bookService.findBookById(id);

		if (owner == null || book == null || book.isBought()) {
			redirectAttributes.addFlashAttribute("messageText", "Error deleting book");
			redirectAttributes.addFlashAttribute("messageDanger", true);
			return "redirect:/bookoverview";
		} else if (bookService.findBookById(id).getOwner().getId().equals(owner.getId())) {
			bookService.deleteBook(id);
			redirectAttributes.addFlashAttribute("messageText", "Book deleted");
			redirectAttributes.addFlashAttribute("messageSuccess", true);
			return "redirect:/bookoverview";
		}

		redirectAttributes.addFlashAttribute("messageText", "Error deleting book");
		redirectAttributes.addFlashAttribute("messageDanger", true);
		return "redirect:/bookoverview";
	}

	@RequestMapping(value = "/sell-book-overview", method = { RequestMethod.GET, RequestMethod.POST })
	public String bookSellOverviewPage(@ModelAttribute("bookFilter") BookFilter bookFilter,
									   @ModelAttribute("bookSortFilter") BookSortFilter bookSortFilter,
									   WebRequest webRequest,
									   Model model,
									   Principal principal) {
		return this.bookSellOverviewPagination(bookFilter, bookSortFilter, webRequest, 1, model, principal);
	}

	@RequestMapping(value = "/sell-book-overview/{page}", method = { RequestMethod.GET, RequestMethod.POST })
	public String bookSellOverviewPagination(@ModelAttribute("bookFilter") BookFilter bookFilter,
											 @ModelAttribute("bookSortFilter") BookSortFilter bookSortFilter,
											 WebRequest webRequest,
											 @PathVariable("page") Integer page,
											 Model model,
											 Principal principal) {
		if (webRequest.getParameter("search-term-filter") != null && webRequest.getParameter("search-term") != null) {
			logger.debug("search-term-filter: " + webRequest.getParameter("search-term-filter"));
			logger.debug("search-term:        " + webRequest.getParameter("search-term"));
		}

		if (webRequest.getParameter("on-sale-filter") != null) {
			logger.debug("on-sale-filter: " + webRequest.getParameter("on-sale-filter"));
		}

		bookFilter.setSearchPage("sell-book-overview");
		Filters.setBookFilter(bookFilter, webRequest);

		if (bookSortFilter.getSortPage() != null && !bookSortFilter.getSortPage().equals("sell-book-overview")) {
			Filters.resetSortFilter(bookSortFilter);
		}
		bookSortFilter.setSortPage("sell-book-overview");
		Filters.setBookSortFilter(bookSortFilter, webRequest);

		Page<Book> booksToSale = bookService.findByToSaleTrueAndBoughtFalseOrderByShowcaseDateTimeDesc(page, paginationPropertiesLoader.getPaginationItemsPerPage("pagination.items.sell-book-overview"), bookFilter, true, bookSortFilter);

		model.addAttribute("books", booksToSale.getContent());
		model.addAttribute("page", booksToSale);
		// oh my god, wtf, this is shit
		try {
			model.addAttribute("authedUser", principal.getName());
		} catch (NullPointerException ex) {
			model.addAttribute("authedUser", "");
		}
		
		return "/sell-book-overview";
	}

}