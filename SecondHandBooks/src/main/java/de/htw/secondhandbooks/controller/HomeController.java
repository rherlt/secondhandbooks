package de.htw.secondhandbooks.controller;

import de.htw.secondhandbooks.model.Notification;
import de.htw.secondhandbooks.service.NotificationService;
import de.htw.secondhandbooks.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.List;
import java.util.Locale;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
    @Autowired
    NotificationService notificationService;
    @Autowired
    UserService userService;

    /**
     * Simply selects the home view to render by returning its name.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Locale locale, Model model, Principal principal) {
        List<Notification> notifications = notificationService.getAllUnreadNotificationsForPrincipal(principal);
        model.addAttribute("notifications", notifications);

        return "home";
    }
    
    /**
     * Simply selects the home view to render by returning its name.
     */
    @RequestMapping(value = "/access-denied", method = RequestMethod.GET)
    public String accessDenied(Model model) {
    	model.addAttribute("messageText", "You are not allowed to access this page");
    	model.addAttribute("messageDanger", true);

        return "home";
    }
}
