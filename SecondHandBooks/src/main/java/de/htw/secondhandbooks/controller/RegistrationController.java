package de.htw.secondhandbooks.controller;

import de.htw.secondhandbooks.dto.UserDTO;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.model.VerificationToken;
import de.htw.secondhandbooks.service.EmailService;
import de.htw.secondhandbooks.service.UserService;
import de.htw.secondhandbooks.utils.Urls;
import de.htw.secondhandbooks.validation.UserDataValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@Controller
public class RegistrationController {

    @Autowired
    UserService userService;

    @Autowired
    EmailService emailService;

    @Autowired
    UserDataValidator validator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String showRegistrationForm(Model model) {
        UserDTO userDTO = new UserDTO();
        model.addAttribute("userDTO", userDTO);
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registerUser(@ModelAttribute("userDTO") @Validated UserDTO userDTO, BindingResult result, final HttpServletRequest request, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "registration";
        } else {
            User user = userService.registerNewUserAccount(userDTO);

            final String appUrl = Urls.getBaseUrl(request);

            final String token = UUID.randomUUID().toString();

            userService.createVerificationTokenForUser(user, token);

            sendRegistrationEmail(userDTO.getEmail(), token, appUrl);
            
            redirectAttributes.addFlashAttribute("messageText", "Your account was successfully created");
            redirectAttributes.addFlashAttribute("messageSuccess", true);
            return "successful-registration";
        }
    }

    @RequestMapping(value = "/registration-confirm", method = RequestMethod.GET)
    public String confirmRegistration(@RequestParam("token") final String token, RedirectAttributes redirectAttributes) {
        final VerificationToken verificationToken = userService.getVerificationToken(token);
        if (verificationToken == null) {
        	redirectAttributes.addFlashAttribute("messageText", "This VerificationToken is invalid");
        	redirectAttributes.addFlashAttribute("messageDanger", true);
            return "redirect:/";
        }

        final User user = verificationToken.getUser();
        user.setEnabled(true);
        userService.saveRegisteredUser(user);
        redirectAttributes.addFlashAttribute("messageText", "Your account is now activated");
        redirectAttributes.addFlashAttribute("messageSuccess", true);

        return "redirect:/";
    }

    private void sendRegistrationEmail(String recipientAddress, String token, String appUrl) {
        String subject = "Registration Confirmation";
        String confirmationUrl = appUrl+ "/registration-confirm?token=" + token;
        String message = "Registration is successful. Please click the following link for verification:";
        String text = message + System.lineSeparator() + System.lineSeparator() + confirmationUrl;
        emailService.sendEmail(recipientAddress, text, subject, false);
    }
}
