package de.htw.secondhandbooks.controller;

import de.htw.secondhandbooks.config.PaginationPropertiesConfig;
import de.htw.secondhandbooks.dto.UserChangeRequestDTO;
import de.htw.secondhandbooks.dto.UserDTO;
import de.htw.secondhandbooks.validation.UserDataContainable;
import de.htw.secondhandbooks.model.ChangeRequest;
import de.htw.secondhandbooks.model.Notification;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.service.ChangeRequestService;
import de.htw.secondhandbooks.service.NotificationService;
import de.htw.secondhandbooks.service.RoleService;
import de.htw.secondhandbooks.service.UserService;
import de.htw.secondhandbooks.validation.UserDataValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@Controller
public class AdminController {
    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @Autowired
    ChangeRequestService changeRequestService;

    @Autowired
    NotificationService notificationService;

    @Autowired
    UserDataValidator userDataValidator;

    @Autowired
    @Qualifier("sessionRegistry")
    SessionRegistry sessionRegistry;

    @Autowired
    PaginationPropertiesConfig paginationPropertiesConfig;

    @InitBinder("userToEdit")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(userDataValidator);
    }

    @RequestMapping(value = "/admin/users", method = RequestMethod.GET)
    public String listAllUsersAdmin(Model model) {
        return this.listAllUsersAdminPagination(1, model);
    }

    @RequestMapping(value = "/admin/users/{page}", method = RequestMethod.GET)
    public String listAllUsersAdminPagination(@PathVariable("page") Integer page, Model model) {
        Page<UserDTO> userDTOPage = userService.findAllUsersWithTheirRolesPageable(page, paginationPropertiesConfig.getPaginationItemsPerPage("pagination.items.users"));
        model.addAttribute("users", userDTOPage.getContent());
        model.addAttribute("page", userDTOPage);
        return "users";
    }

    @RequestMapping(value = "/admin/users", method = RequestMethod.POST)
    public String updateUserAdmin(@ModelAttribute("userToEdit") @Validated UserDTO userDTO, BindingResult result, Principal principal, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "user-form";
        } else {

            User user = userService.findUserById(userDTO.getId());

            userDTO.setChangedRole(true);
            userService.updateUser(userDTO);

            if (user.getShowcaseLocked() != userDTO.isShowcaseLocked()) {
                String message;
                String title;
                if (userDTO.isShowcaseLocked()) {
                    title = "Your showcase got locked.";
                    message = "Your showcase is locked, you're not able to sell books anymore.";
                } else {
                    title = "Your showcase got locked.";
                    message = "Your showcase is unlocked, you can sell books again.";
                }
                User sender = userService.findUserByEmail(principal.getName());
                Notification notification = new Notification(sender, user, title, message);
                notificationService.persistNotification(notification);
            }

            this.checkForChangedRoles(user, userDTO);
            this.checkForChangedEmailAddress(user, userDTO);

            redirectAttributes.addFlashAttribute("messageText", "User successfully edited");
            redirectAttributes.addFlashAttribute("messageSuccess", true);
            return "redirect:/admin/users";
        }
    }

    @RequestMapping(value = "/admin/user/{userId}/edit", method = RequestMethod.GET)
    public String showUserFormAdmin(Model model, @PathVariable Long userId) {
        User user = userService.findUserById(userId);

        model.addAttribute("userToEdit", new UserDTO(user, user.getRoles().contains(roleService.findRoleByRole("ADMIN")), user.getEnabled(), user.getShowcaseLocked(), user.getNotificationsViaEmail()));

        return "user-form";
    }

    @RequestMapping(value = "/admin/change-request/{changeRequestId}", method = RequestMethod.GET)
    public String changeRequestApprovalAdmin(Model model, @PathVariable Long changeRequestId) {
        ChangeRequest changeRequest = changeRequestService.findChangeRequestById(changeRequestId);

        if (!changeRequest.isDeletionRequest()) {
            model.addAttribute("oldData", new UserChangeRequestDTO(changeRequest.getSender()));
            model.addAttribute("newData", new UserChangeRequestDTO(changeRequest));
            model.addAttribute("changeRequestId", changeRequestId);

            return "change-request-approval";
        } else {
            return "redirect:/notifications";
        }
    }

    @RequestMapping(value = "/admin/change-request/{changeRequestId}", method = RequestMethod.POST)
    public String handleChangeRequest(@ModelAttribute("newData") UserChangeRequestDTO newData,
                                      @RequestParam(value = "action", required = true) String action,
                                      @PathVariable Long changeRequestId, Principal principal,
                                      HttpServletRequest request, RedirectAttributes redirectAttributes) {
        Notification notification;
        String message;
        ChangeRequest changeRequest = changeRequestService.findChangeRequestById(changeRequestId);
        User thisAdmin = userService.findUserByEmail(principal.getName());
        User userToEdit = userService.findUserById(newData.getId());

        if (action.equals("Approve")) {
            userService.updateUser(newData);
            this.checkForChangedEmailAddress(userToEdit, newData);
            message = "The user " + userToEdit.getDisplayName() + " was successfully edited.";
            notification = notificationService.createNotificationForChangeRequestApproval(thisAdmin, userToEdit, request);
        } else {
            message = "The change request for " + userToEdit.getDisplayName() + " was declined.";
            notification = notificationService.createNotificationForChangeRequestDenial(thisAdmin, userToEdit, newData.getReason(), request);
        }

        notificationService.persistNotification(notification);
        changeRequestService.deleteChangeRequest(changeRequest);

        redirectAttributes.addFlashAttribute("messageText", message);
        redirectAttributes.addFlashAttribute("messageSuccess", true);
        return "redirect:/notifications";
    }

    @RequestMapping(value = "/admin/deletion-request/{deletionRequestId}", method = RequestMethod.GET)
    public String showDeletionRequestApprovalForm(Model model, @PathVariable Long deletionRequestId) {
        ChangeRequest deletionRequest = changeRequestService.findChangeRequestById(deletionRequestId);

        if (deletionRequest.isDeletionRequest()) {
            model.addAttribute("userToDelete", new UserDTO(deletionRequest.getSender()));
            model.addAttribute("deletionRequestId", deletionRequestId);

            return "delete-account-approval";
        } else {
            return "redirect:/notifications";
        }
    }

    @RequestMapping(value = "/admin/deletion-request/{deletionRequestId}", method = RequestMethod.POST)
    public String handleDeletionRequest(@ModelAttribute("userToDelete") UserChangeRequestDTO userToDelete,
                                        @RequestParam(value = "action", required = true) String action,
                                        @PathVariable Long deletionRequestId, Principal principal,
                                        HttpServletRequest request, RedirectAttributes redirectAttributes) {
        String message;
        User user = userService.findUserById(userToDelete.getId());
        if (action.equals("Approve")) {
            this.forceLogout(user);
            userService.deleteUser(userToDelete.getId());
            message = "The user " + user.getDisplayName() + " was successfully deleted.";
        } else {
            User sender = userService.findUserByEmail(principal.getName());
            User receiver = userService.findUserById(userToDelete.getId());
            message = "The deletion request for " + user.getDisplayName() + " was declined.";
            Notification notification = notificationService.createNotificationForDeletionRequestDenial(sender, receiver, userToDelete.getReason(), request);
            notificationService.persistNotification(notification);

            ChangeRequest deletionRequest = changeRequestService.findChangeRequestById(deletionRequestId);
            changeRequestService.deleteChangeRequest(deletionRequest);
        }

        redirectAttributes.addFlashAttribute("messageText", message);
        redirectAttributes.addFlashAttribute("messageSuccess", true);
        return "redirect:/notifications";
    }

    private void checkForChangedRoles(User user, UserDTO userDTO) {
        if ((user.getRoles().contains(roleService.findRoleByRole("ADMIN")) && !userDTO.isAdmin())
                || (!user.getRoles().contains(roleService.findRoleByRole("ADMIN")) && userDTO.isAdmin())) {
            this.forceLogout(user);
        }
    }

    private void checkForChangedEmailAddress(User user, UserDataContainable newUserData) {
        if (!user.getEmail().equals(newUserData.getEmail())) {
            this.forceLogout(user);
        }
    }

    private void forceLogout(User user) {
        List<Object> principals = sessionRegistry.getAllPrincipals();
        for (Object sessionPrincipal : principals) {
            if (sessionPrincipal instanceof org.springframework.security.core.userdetails.User) {
                if (((org.springframework.security.core.userdetails.User) sessionPrincipal).getUsername().equals(user.getEmail())) {
                    List<SessionInformation> sessions = sessionRegistry.getAllSessions(sessionPrincipal, true);
                    for (SessionInformation session : sessions) {
                        session.expireNow();
                    }
                }
            }
        }
    }
}
