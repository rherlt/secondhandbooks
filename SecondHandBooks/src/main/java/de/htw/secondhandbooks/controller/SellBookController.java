package de.htw.secondhandbooks.controller;

import de.htw.secondhandbooks.dto.BookSellDTO;
import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.Notification;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.service.BookService;
import de.htw.secondhandbooks.service.NotificationService;
import de.htw.secondhandbooks.service.UserService;
import de.htw.secondhandbooks.validation.BookSellingValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.sql.Date;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;

@Controller
public class SellBookController {

	private static final Logger logger = LoggerFactory.getLogger(SellBookController.class);

	@Autowired
	BookService bookService;
	@Autowired
	UserService userService;
	@Autowired
	NotificationService notificationService;
	@Autowired
	BookSellingValidator validator;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@RequestMapping(value = "/accept-book-offer/{bookId}", method = RequestMethod.GET)
	public String sellBookAcceptOffer(@PathVariable("bookId") long id, Principal principal, HttpServletRequest request) {
		User owner = userService.findUserByEmail(principal.getName());
		Book book = bookService.findBookById(id);

		if (!bookBelongsToUser(owner, book) || owner.getShowcaseLocked()) {
			return "redirect:/access-denied";
		}

		// TODO: Maybe add some better error handling?
		if (book.getHighestOffer() == 0 || book.getHighestOfferUser() == null || book.isBought()) {
			return "redirect:/access-denied";
		}

		book.setBought(true);
		bookService.updateBook(book);

		// TODO Maybe should be changed.
		Notification notification = notificationService.createNotificationForPayment(owner,
				book.getHighestOfferUser(), book, request);
		notificationService.persistNotification(notification);

		return "redirect:/bookoverview";
	}

	@RequestMapping(value = "/sell-book/{bookId}", method = RequestMethod.GET)
	public String sellBookForm(@PathVariable("bookId") long id, Model model, Principal principal,
							   HttpServletResponse response, BookSellDTO bookSellDto,
							   RedirectAttributes redirectAttributes) {

		User owner = userService.findUserByEmail(principal.getName());
		Book book = bookService.findBookById(id);

		if (!bookBelongsToUser(owner, book)) {
			redirectAttributes.addFlashAttribute("messageText", "Access denied");
			redirectAttributes.addFlashAttribute("messageDanger", true);
		}

		if (owner.getShowcaseLocked()) {
			redirectAttributes.addFlashAttribute("messageText", "Your showcase is locked. Check your notifications");
			redirectAttributes.addFlashAttribute("messageInfo", true);
		}

		if (owner.getPayPalEmail().equals("")) {
			redirectAttributes.addFlashAttribute("messageText", "You need to add a PayPal Emailaddress to sell books. Please update your profile");
			redirectAttributes.addFlashAttribute("messageInfo", true);
		}

		if (!bookBelongsToUser(owner, book) || owner.getShowcaseLocked() || owner.getPayPalEmail().equals("")) {
			return "redirect:/bookoverview";
		}

		String base64Image = book.getImageBase64();
		
		if (base64Image != null) {			
			model.addAttribute("bookBase64Image", book.getImageBase64());
		}
		
		bookSellDto.setBookData(book);
		bookSellDto.setSellingData(book);
		model.addAttribute("bookSellDto", bookSellDto);

		return "sell-book";
	}

	@RequestMapping(value = "/sell-book/{bookId}", method = RequestMethod.POST)
	public ModelAndView sellBook(@PathVariable("bookId") long id,
								 @ModelAttribute("bookSellDto") @Validated BookSellDTO bookSellDto,
								 BindingResult result,
								 Model model,
								 Principal principal) {

		logger.debug("sellBook() is called with id <{}>", id);

		User owner = userService.findUserByEmail(principal.getName());
		Book book = bookService.findBookById(id);

		bookSellDto.updateBookData(book);
		model.addAttribute("bookSellDto", bookSellDto);

		
		
		if (owner.getShowcaseLocked() || owner.getPayPalEmail().equals("")) {
			return new ModelAndView("redirect:/access-denied");
		}

		if (!bookBelongsToUser(owner, book))
			return new ModelAndView("redirect:/bookoverview");
		if (book.isBought())
			return new ModelAndView("redirect:/bookoverview");
		//if (book.getHighestOffer() >= Double.valueOf(bookSellDto.getPrice()))
		//return new ModelAndView("redirect:/bookoverview"); // TODO some error page

		logger.debug("sellBook() owner.getId() <{}>, book.getId() <{}>, book.getOwner.getId() <{}>", owner.getId(), book.getId(), book.getOwner().getId());

		if (result.hasErrors()) {
			return new ModelAndView("sell-book", "BindingAwareModelMap", model);
		} else {
			book.setPrice(Double.valueOf(bookSellDto.getPrice()));
			book.setShippingCost(Double.valueOf(bookSellDto.getShippingCost()));
			book.setToSale(true);
			book.setShowcaseDateTime(Date.from(Instant.now()));
			owner.setLatestShowcaseDateTime(Date.from(Instant.now()));

			bookService.updateBook(book);
			userService.updateUser(owner);

			return new ModelAndView("redirect:/bookoverview");
		}
	}

	@RequestMapping(value = "/sell-book-cancel/{bookId}", method = RequestMethod.GET)
	public String sellBookCancel(@PathVariable("bookId") long id, Principal principal) {
		User owner = userService.findUserByEmail(principal.getName());
		Book book = bookService.findBookById(id);

		if (owner.getShowcaseLocked()) {
			return "redirect:/access-denied";
		}

		if (!bookBelongsToUser(owner, book) || !book.isToSale() || book.isBought())
			return "redirect:/bookoverview";

		book.setHighestOffer(0);
		book.setToSale(false);

		if (book.getHighestOfferUser() != null) {
			User offerUser = book.getHighestOfferUser();
			offerUser.removeOffer(book);
			book.setHighestOfferUser(null);
		}

		bookService.updateBook(book);

		return "redirect:/bookoverview";
	}

	private boolean bookBelongsToUser(User owner, Book book) {
		return book.getOwner().getId().equals(owner.getId());
	}
}
