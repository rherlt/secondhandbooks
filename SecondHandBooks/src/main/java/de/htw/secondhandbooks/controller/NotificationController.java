package de.htw.secondhandbooks.controller;

import de.htw.secondhandbooks.config.PaginationPropertiesConfig;
import de.htw.secondhandbooks.dto.NotificationDTO;
import de.htw.secondhandbooks.dto.NotificationDTOListWrapper;
import de.htw.secondhandbooks.model.Notification;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.service.NotificationService;
import de.htw.secondhandbooks.service.UserService;
import de.htw.secondhandbooks.util.NotificationComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
public class NotificationController {

    @Autowired
    NotificationService notificationService;
    @Autowired
    UserService userService;
    @Autowired
    PaginationPropertiesConfig paginationPropertiesConfig;

    @RequestMapping(value = "/notification/email", method = RequestMethod.GET)
    public String setNotificationViaEmail(Principal principal) {
        User user = userService.findUserByEmail(principal.getName());

        if (user != null) {
        	if (user.getNotificationsViaEmail()) {
        		user.setNotificationsViaEmail(false);
        	} else {
        		user.setNotificationsViaEmail(true);
        	}
        	userService.updateUser(user);
        	return "redirect:/notifications";
        }

        return "redirect:/access-denied";
    }

    @RequestMapping(value = "/notification/{id}/read", method = RequestMethod.GET)
    public String setNotificationAsRead(@PathVariable("id") long id, Principal principal) {
        User user = userService.findUserByEmail(principal.getName());
        Notification notification = notificationService.findNotificationById(id);

        List<User> receivers = notification.getReceivers();
        for (User receiver : receivers) {
            if (receiver.getId().equals(user.getId())) {
                notificationService.setNotificationAsRead(notification);
                return "redirect:/notifications";
            }
        }

        return "redirect:/access-denied";
    }

    @RequestMapping(value = "/notifications", method = RequestMethod.GET)
    public String overview(@RequestParam(value = "display", required = false) final String display,
                           Model model, Principal principal) {
        return this.overviewPagination(display, 1, model, principal);
    }

    @RequestMapping(value = "/notifications/{page}", method = RequestMethod.GET)
    public String overviewPagination(@RequestParam(value = "display", required = false) final String display,
                           @PathVariable("page") Integer page,
                           Model model, Principal principal) {
        User user = userService.findUserByEmail(principal.getName());

        Page<Notification> notifications;
        NotificationDTOListWrapper wrapper = new NotificationDTOListWrapper();

        if (null != display && display.equals("oldNotifications")) {
            notifications = notificationService.getAllReadNotificationsForUser(user, page, paginationPropertiesConfig.getPaginationItemsPerPage("pagination.items.notifications"));
        } else {
            notifications = notificationService.getAllUnreadNotificationsForUser(user, page, paginationPropertiesConfig.getPaginationItemsPerPage("pagination.items.notifications"));
        }

        for (Notification notification : notifications.getContent()) {
            wrapper.add(new NotificationDTO(notification));
        }

        int newNotificationCount = notificationService.getAllUnreadNotificationsForUser(user).size();
        int oldNotificationCount = notificationService.getAllReadNotificationsForUser(user).size();

        model.addAttribute("notificationDTOListWrapper", wrapper);
        model.addAttribute("display", display);
        model.addAttribute("oldNotificationCount", oldNotificationCount);
        model.addAttribute("newNotificationCount", newNotificationCount);
        model.addAttribute("emailNotifications", user.getNotificationsViaEmail());
        model.addAttribute("page", notifications);

        return "notifications";
    }

    @RequestMapping(value = "/notifications", method = RequestMethod.POST)
    public String markSelectedAsRead(@ModelAttribute("notificationDTOListWrapper") NotificationDTOListWrapper wrapper,
                                     @RequestParam(value = "action", required = true) String action) {
        if (action.equals("Mark selected as read")) {
            for (NotificationDTO notificationDTO : wrapper.getNotificationDTOList()) {
                if (notificationDTO.isRead()) {
                    Notification notification = notificationService.findNotificationById(notificationDTO.getId());
                    notificationService.setNotificationAsRead(notification);
                }
            }
            return "redirect:/notifications";
        }else {
            for (NotificationDTO notificationDTO : wrapper.getNotificationDTOList()) {
                Notification notification = notificationService.findNotificationById(notificationDTO.getId());
                notificationService.deleteNotification(notification);
            }
            return "redirect:/notifications?display=oldNotifications";
        }
    }
}