package de.htw.secondhandbooks.controller;

import de.htw.secondhandbooks.config.PaginationPropertiesConfig;
import de.htw.secondhandbooks.filter.BookFilter;
import de.htw.secondhandbooks.filter.BookSortFilter;
import de.htw.secondhandbooks.filter.UserFilter;
import de.htw.secondhandbooks.dto.UserReportDTO;
import de.htw.secondhandbooks.filter.UserSortFilter;
import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.Notification;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.service.BookService;
import de.htw.secondhandbooks.service.NotificationService;
import de.htw.secondhandbooks.service.UserService;
import de.htw.secondhandbooks.service.RatingService;
import de.htw.secondhandbooks.utils.Filters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@SessionAttributes({"bookFilter", "bookSortFilter", "userFilter", "userSortFilter"})
public class ShowcaseController {

	@Autowired
	BookService bookService;
	@Autowired
	UserService userService;
	@Autowired
	RatingService ratingService;
	@Autowired
	PaginationPropertiesConfig paginationPropertiesConfig;

    @Autowired
    NotificationService notificationService;

	@ModelAttribute("bookFilter")
	public BookFilter initBookFilter() {
		return new BookFilter();
	}


    @ModelAttribute("userFilter")
    public UserFilter initUserFilter() {
        return new UserFilter();
    }
    
    @ModelAttribute("userSortFilter")
    public UserSortFilter initUserSortFilter() {
        return new UserSortFilter();
    }

    @ModelAttribute("bookSortFilter")
    public BookSortFilter initBookSortFilter() {
        return new BookSortFilter();
    }


	@RequestMapping(value = "/showcaseoverview", method = { RequestMethod.GET, RequestMethod.POST })
	public String ownShowcase(@ModelAttribute("bookFilter") BookFilter bookFilter,
								@ModelAttribute("bookSortFilter") BookSortFilter bookSortFilter,
									   WebRequest webRequest,
									   Model model,
									   Principal principal) {

		Long id = userService.findUserByEmail(principal.getName()).getId();
		return this.showcaseOverviewPage(bookFilter,bookSortFilter, webRequest, id, model, principal);
	}

	@RequestMapping(value = "/showcaseoverview/{id}", method = { RequestMethod.GET, RequestMethod.POST })
	public String showcaseOverviewPage(@ModelAttribute("bookFilter") BookFilter bookFilter,
                                       @ModelAttribute("bookSortFilter") BookSortFilter bookSortFilter,
									   WebRequest webRequest,
									   @PathVariable("id") long id,
									   Model model,
									   Principal principal) {
		return this.showcaseOverviewPagination(bookFilter, bookSortFilter, webRequest, id, 1, model,principal);
	}

	@RequestMapping(value = "/showcaseoverview/{id}/page/{page}", method = { RequestMethod.GET, RequestMethod.POST })
	public String showcaseOverviewPagination(@ModelAttribute("bookFilter") BookFilter bookFilter,
                                             @ModelAttribute("bookSortFilter") BookSortFilter bookSortFilter,
											 WebRequest webRequest,
											 @PathVariable("id") long id,
											 @PathVariable("page") Integer page,
											 Model model, Principal principal) {

        bookFilter.setSearchPage("showcaseoverview");
		Filters.setBookFilter(bookFilter, webRequest);

        if (bookSortFilter.getSortPage() != null && !bookSortFilter.getSortPage().equals("sell-book-overview")) {
            Filters.resetSortFilter(bookSortFilter);
        }
        bookSortFilter.setSortPage("showcaseoverview");
        Filters.setBookSortFilter(bookSortFilter, webRequest);

		User showcaseOwner = userService.findUserById(id);

        if (showcaseOwner.getShowcaseLocked()) {
            return "redirect:/access-denied";
        }

		Page<Book> booksPage = bookService.findAllByOwnerIdAndToSaleIsTrueAndBoughtIsFalse(showcaseOwner.getId(), page, paginationPropertiesConfig.getPaginationItemsPerPage("pagination.items.showcaseoverview"), bookFilter, false, bookSortFilter);

		model.addAttribute("rating", showcaseOwner.getRating());
        model.addAttribute("books", booksPage.getContent());
        model.addAttribute("showcaseOwner", showcaseOwner);
        model.addAttribute("page", booksPage);
        model.addAttribute("userToReport", new UserReportDTO(showcaseOwner));

        // oh my god, wtf, this is shit
        try {
            model.addAttribute("authedUser", principal.getName());
        } catch (NullPointerException ex) {
            model.addAttribute("authedUser", "");
        }
        return "showcaseoverview";
    }

    @RequestMapping(value = "/showcaseoverview/report/{id}", method = RequestMethod.POST)
    public String updateUserAdmin(@PathVariable("id") Long id,
                                  @ModelAttribute("bookFilter") BookFilter bookFilter,
                                  @ModelAttribute("bookSortFilter") BookSortFilter bookSortFilter,
                                  WebRequest webRequest,
                                  @ModelAttribute("userToReport") UserReportDTO userToReportDTO,
                                  Model model, Principal principal, HttpServletRequest request) {

        User sender = userService.findUserByEmail(principal.getName());
        Notification notification = notificationService.createNotificationForShowcaseReport(sender, userToReportDTO, request);
        notificationService.persistNotification(notification);

        model.addAttribute("messageSuccess", true);
        model.addAttribute("messageText", "Report successfully sent.");

        return showcaseOverviewPage(bookFilter, bookSortFilter, webRequest, id, model, principal);
    }


	@RequestMapping(value = "/showcases", method = { RequestMethod.GET, RequestMethod.POST })
	public String showcaseOverviewPage(@ModelAttribute("userFilter") UserFilter userFilter,
                                       @ModelAttribute("userSortFilter") UserSortFilter userSortFilter,
									   WebRequest webRequest,
									   Model model, Principal principal) {
		return this.showcaseOverviewPagination(userFilter, userSortFilter, webRequest, 1, model, principal);
	}

	@RequestMapping(value = "/showcases/{page}", method = { RequestMethod.GET, RequestMethod.POST })
	public String showcaseOverviewPagination(@ModelAttribute("userFilter") UserFilter userFilter,
                                             @ModelAttribute("userSortFilter") UserSortFilter userSortFilter,
											 WebRequest webRequest,
											 @PathVariable("page") Integer page,
											 Model model,
											 Principal principal) {
		userFilter.setSearchPage("showcases");
		Filters.setUserFilter(userFilter, webRequest);
        Filters.setUserSortFilter(userSortFilter, webRequest);

		Page<User> showcasesList = userService.findAllUsersWithShowcase(page, paginationPropertiesConfig.getPaginationItemsPerPage("pagination.items.showcases"), userFilter, userSortFilter);
		List<String> showcaseNameList = new ArrayList<>();

		for (int i = 0; i < showcasesList.getContent().size(); i++) {
			showcaseNameList.add(showcasesList.getContent().get(i).getDisplayName());
		}

        model.addAttribute("showcases", showcasesList.getContent());
        model.addAttribute("showcasesNames", showcaseNameList);
        model.addAttribute("page", showcasesList);

        // oh my god, wtf, this is shit
        try {
            model.addAttribute("authedUser", principal.getName());
        } catch (NullPointerException ex) {
            model.addAttribute("authedUser", "");
        }
        return "showcases";
    }
}
