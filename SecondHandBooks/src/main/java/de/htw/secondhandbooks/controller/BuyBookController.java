package de.htw.secondhandbooks.controller;

import de.htw.secondhandbooks.dto.BookBuyDTO;
import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.Notification;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.service.BookService;
import de.htw.secondhandbooks.service.NotificationService;
import de.htw.secondhandbooks.service.PayPalPaymentService;
import de.htw.secondhandbooks.service.UserService;
import de.htw.secondhandbooks.validation.BookBuyingValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@Controller
public class BuyBookController {

    @Autowired
    BookService bookService;
    @Autowired
    UserService userService;
    @Autowired
    PayPalPaymentService payPalPaymentService;
    @Autowired
    NotificationService notificationService;
    @Autowired
    BookBuyingValidator validator;

    @InitBinder("bookToBuy")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @RequestMapping(value = "/offersoverview", method = RequestMethod.GET)
    public String offersOverview(Model model, Principal principal) {
        User user = userService.findUserByEmail(principal.getName());

        model.addAttribute("user", user);
        return "offersoverview";
    }

    @RequestMapping(value = "/buy-book/{bookId}", method = RequestMethod.GET)
    public String sellBookForm(@PathVariable("bookId") long id,
                               Model model,
                               Principal principal,
                               HttpServletResponse response,
                               BookBuyDTO bookBuyDto) {
        User user = null;
        if (principal != null) {
            user = userService.findUserByEmail(principal.getName());
        }
        Book book = bookService.findBookById(id);

        String base64Image = book.getImageBase64();

        if (base64Image != null) {
            model.addAttribute("bookBase64Image", book.getImageBase64());
        }

        if (book == null || !book.isToSale())
            return "redirect:/access-denied";

        if (book.isBought()) {
            User highestOfferUser = book.getHighestOfferUser();
            if (book.getHighestOfferUser() != null) {
                if (user.getId() != highestOfferUser.getId() && user.getId() != book.getOwner().getId())
                    return "redirect:/access-denied";
            }
        }

        bookBuyDto.setBookData(book);
        bookBuyDto.setBuyingData(book, user);
        model.addAttribute("bookToBuy", bookBuyDto);
        model.addAttribute("buyTab", false);

        return "buy-book";
    }

    @RequestMapping(value = "/buy-book-fixed-price/{bookId}", method = RequestMethod.GET)
    public String sellBook(@PathVariable("bookId") long id, Principal principal, HttpServletRequest request) {
        User user = userService.findUserByEmail(principal.getName());
        Book book = bookService.findBookById(id);

        if (book == null || user == null)
            return "redirect:/offersoverview";
        if (!book.isToSale() || book.getOwner().getId().equals(user.getId()) || book.isBought())
            return "redirect:/offersoverview";

        

        book.setBought(true);
        
        if(book.hasOffer() && book.getHighestOfferUser() != user){
        	Notification notification2 = notificationService.createNotificationForOfferOutbid(user, book.getHighestOfferUser(), book, request);
            notificationService.persistNotification(notification2);
        }
        
        book.setHighestOffer(book.getPrice());
        book.setHighestOfferUser(user);
        if (!book.getHighestOfferUser().equals(user)) {
            user.addOffer(book);
        }

        
        userService.createOrUpdateUser(user);
        bookService.updateBook(book);

        Notification notification = notificationService.createNotificationForPayment(book.getOwner(), user, book, request);
        notificationService.persistNotification(notification);

        return "redirect:/offersoverview";
    }

    @RequestMapping(value = "/cancel-offer/{bookId}", method = RequestMethod.GET)
    public String cancelOffer(@PathVariable("bookId") long id, Principal principal, HttpServletRequest request) {
        User user = userService.findUserByEmail(principal.getName());
        Book book = bookService.findBookById(id);

        if (book == null || user == null)
            return "redirect:/offersoverview";
        if (!book.isToSale() || book.getOwner().getId().equals(user.getId()) || book.getHighestOfferUser() == null)
            return "redirect:/offersoverview";
        if (!book.getHighestOfferUser().getId().equals(user.getId()))
            return "redirect:/offersoverview";

        book.setHighestOffer(0);
        book.setHighestOfferUser(null);
        book.setBought(false);
        user.removeOffer(book);

        userService.createOrUpdateUser(user);
        bookService.updateBook(book);

        Notification notification = notificationService.createNotificationForOfferCanceled(user, book.getOwner(), book, request);
        notificationService.persistNotification(notification);

        return "redirect:/offersoverview";
    }

    @RequestMapping(value = "/buy-book/{bookId}", method = RequestMethod.POST)
    public String makeOffer(@PathVariable("bookId") long id,
                                  @ModelAttribute("bookToBuy") @Validated BookBuyDTO bookBuyDto,
                                  BindingResult result,
                                  Model model,
                                  Principal principal,
                                  HttpServletRequest request) {
        
        if (result.hasErrors()) {
            Book book = bookService.findBookById(id);
            bookBuyDto.setBookData(book);
            String base64Image = book.getImageBase64();

            if (base64Image != null) {
                model.addAttribute("bookBase64Image", book.getImageBase64());
            }
            model.addAttribute("buyTab", true);
            return "buy-book";
        } else {
            User user = userService.findUserByEmail(principal.getName());
            Book book = bookService.findBookById(id);

            bookBuyDto.updateBookData(book, user);

            if (book == null || user == null) {
                return "redirect:/offersoverview";
            }
            if (!book.isToSale() || Double.valueOf(bookBuyDto.getOfferedPrice()) <= book.getHighestOffer()
                    || book.getOwner().getId().equals(user.getId())) {
                return "redirect:/offersoverview";
            }

            Notification notification = notificationService.createNotificationForOfferMade(user, book.getOwner(), book, request);
            notificationService.persistNotification(notification);
            
            if(book.hasOffer() && book.getHighestOfferUser() != user){
            	Notification notification2 = notificationService.createNotificationForOfferOutbid(user, book.getHighestOfferUser(), book, request);
                notificationService.persistNotification(notification2);
            }

            book.setHighestOffer(Double.valueOf(bookBuyDto.getOfferedPrice()));
            book.setHighestOfferUser(user);
            user.addOffer(book);

            bookService.updateBook(book);
            userService.createOrUpdateUser(user);

            return "redirect:/offersoverview";
        }
    }

}
