package de.htw.secondhandbooks.controller;

import de.htw.secondhandbooks.config.PaginationPropertiesConfig;
import de.htw.secondhandbooks.dto.PasswordChangeDTO;
import de.htw.secondhandbooks.dto.UserChangeRequestDTO;
import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.ChangeRequest;
import de.htw.secondhandbooks.model.Notification;
import de.htw.secondhandbooks.model.Rating;
import de.htw.secondhandbooks.model.Role;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.service.ChangeRequestService;
import de.htw.secondhandbooks.service.NotificationService;
import de.htw.secondhandbooks.service.RatingService;
import de.htw.secondhandbooks.service.RoleService;
import de.htw.secondhandbooks.service.UserService;
import de.htw.secondhandbooks.validation.PasswordChangeValidator;
import de.htw.secondhandbooks.validation.UserDataValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
public class ProfileController {

    private static final Logger logger = LoggerFactory.getLogger(ProfileController.class);

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @Autowired
    RatingService ratingService;

    @Autowired
    ChangeRequestService changeRequestService;

    @Autowired
    NotificationService notificationService;

    @Autowired
    UserDataValidator userDataValidator;

    @Autowired
    PasswordChangeValidator passwordChangeValidator;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    @Qualifier("sessionRegistry")
    SessionRegistry sessionRegistry;

    @Autowired
    PaginationPropertiesConfig paginationPropertiesConfig;

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String ownProfile(Model model, Principal principal) {
        Long id = userService.findUserByEmail(principal.getName()).getId();
        return this.showUserProfile(id, model, principal);
    }

    @RequestMapping(value = "/user/{id}/profile", method = RequestMethod.GET)
    public String showUserProfile(@PathVariable("id") Long userId, Model model, Principal principal) {
        User user = userService.findUserById(userId);
        Set<Book> booksOnSale = new HashSet<>(user.getBooksOnSale());

        model.addAttribute("user", user);
        model.addAttribute("booksToSaleCount", booksOnSale.size());
        try {
            model.addAttribute("authedUser", principal.getName());
        } catch (NullPointerException ex) {
            model.addAttribute("authedUser", "");
        }

        return "profile";
    }

    @RequestMapping(value = "/edit-profile", method = RequestMethod.GET)
    public String showChangeRequestForm(Model model, Principal principal) {
    	User user = userService.findUserByEmail(principal.getName());
        model.addAttribute("user", user);
        Role adminRole = roleService.findRoleByRole("ADMIN");

        if (!user.getRoles().contains(adminRole) && !model.containsAttribute("messageText")) {
            model.addAttribute("messageText", "If you want to change or delete your account, a request gets sent to be validated by the admins. You get informed about the outcome.");
            model.addAttribute("messageWarning", true);
        }
        
    	if (!model.containsAttribute("changeRequest")) {
    		model.addAttribute("changeRequest", new UserChangeRequestDTO(user));
    	}
    	if (!model.containsAttribute("passwordChangeDTO")) {
    		PasswordChangeDTO passwordChangeDTO = new PasswordChangeDTO();
            model.addAttribute("passwordChangeDTO", passwordChangeDTO);
    	}
        System.out.println(model);

        return "change-request-form";
    }

    @RequestMapping(value = "/edit-profile", method = RequestMethod.POST)
    public String sendChangeRequest(@ModelAttribute("changeRequest") UserChangeRequestDTO userChangeRequestDTO,
                                    BindingResult result, HttpServletRequest request,
                                    RedirectAttributes redirectAttributes, Model model,
                                    @RequestParam(value = "action", required = true) String action) {

        userDataValidator.validate(userChangeRequestDTO, result);
        logger.debug("sendChangeRequest() POST");
        if (result.hasErrors()) {
            logger.info("sendChangeRequest() has errors" + result.getAllErrors());
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.changeRequest", result);
            redirectAttributes.addFlashAttribute("changeRequest", userChangeRequestDTO);
            return "redirect:/edit-profile";
        } else {
            logger.info("sendChangeRequest() create change request");

            ChangeRequest changeRequest = changeRequestService.createChangeRequest(userChangeRequestDTO);
            String messageText;
            if (action.equals("Delete Account")) {
                changeRequest.setAsDeletionRequest();
                messageText = "A request to delete your account has been sent do be validated by admins.";
            } else {
                messageText = "A request to change your profile has been sent to be validated by admins.";
            }
            changeRequestService.persistChangeRequest(changeRequest);

            Notification notification = notificationService.createNotificationForChangeRequest(changeRequest, request);
            notificationService.persistNotification(notification);

            changeRequest.setNotification(notification);
            changeRequestService.persistChangeRequest(changeRequest);

            logger.info("sendChangeRequest() redirect and add attributes ...");
            redirectAttributes.addFlashAttribute("messageText", messageText);
            redirectAttributes.addFlashAttribute("messageSuccess", true);
            return "redirect:/edit-profile";
        }
    }

    @RequestMapping(value = "/change-password", method = RequestMethod.POST)
    public String changePassword(@ModelAttribute("passwordChangeDTO") PasswordChangeDTO passwordChangeDTO,
                                 BindingResult result, Principal principal, Model model, RedirectAttributes redirectAttributes) {

    	User user = userService.findUserByEmail(principal.getName());
        passwordChangeValidator.validate(new PasswordChangeValidator.PasswordChangeValidatorContainer(passwordChangeDTO, user.getPassword()), result);

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.passwordChangeDTO", result);
            redirectAttributes.addFlashAttribute("passwordChangeDTO", passwordChangeDTO);
            return "redirect:/edit-profile";
        }

        user.setPassword(passwordEncoder.encode(passwordChangeDTO.getNewPassword()));

        userService.updateUser(user);

        redirectAttributes.addFlashAttribute("messageText", "Your password was successfully changed");
        redirectAttributes.addFlashAttribute("messageSuccess", true);
        return "redirect:/edit-profile";
    }

    @RequestMapping(value = "/ratings", method = RequestMethod.GET)
    public String ownRatings(Model model, Principal principal){
        Long id = userService.findUserByEmail(principal.getName()).getId();
        return this.showRatingsForUser(id, model, principal);
    }

    @RequestMapping(value = "/user/{id}/ratings", method = RequestMethod.GET)
    public String showRatingsForUser(@PathVariable("id") long sellerId,
                                     Model model, Principal principal){
        return this.showRatingsForUserPagination(sellerId, 1, model, principal);
    }

    @RequestMapping(value = "/user/{id}/ratings/page/{page}", method = RequestMethod.GET)
    public String showRatingsForUserPagination(@PathVariable("id") long sellerId,
                                     @PathVariable("page") Integer page,
                                     Model model, Principal principal){

        User seller = userService.findUserById(sellerId);

        Page<Rating> ratingForSeller = ratingService.findAllBySeller(seller, page, paginationPropertiesConfig.getPaginationItemsPerPage("pagination.items.ratings"));

        model.addAttribute("seller", seller);
        model.addAttribute("sellerRatings", ratingForSeller.getContent());
        model.addAttribute("page", ratingForSeller);
        try {
            model.addAttribute("authedUser", principal.getName());
        } catch (NullPointerException ex) {
            model.addAttribute("authedUser", "");
        }

        return "seller-ratings";
    }
}
