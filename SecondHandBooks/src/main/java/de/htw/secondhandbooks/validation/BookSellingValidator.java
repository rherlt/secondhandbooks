package de.htw.secondhandbooks.validation;

import de.htw.secondhandbooks.dto.BookSellDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class BookSellingValidator implements Validator {

    @Autowired
    PriceValidator priceValidator;

    @Override
    public boolean supports(Class<?> clazz) {
        return BookSellDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        BookSellDTO bookSellDto = (BookSellDTO) target;
        bookSellDto.setPrice(bookSellDto.getPrice().replace(',', '.').trim());
        bookSellDto.setShippingCost(bookSellDto.getShippingCost().replace(',', '.').trim());

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "NotEmpty.price");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shippingCost", "NotEmpty.shippingCost");

        if(!priceValidator.validatePrice(bookSellDto.getPrice())){
            errors.rejectValue("price", "Pattern.price");
        }

        if(!priceValidator.validatePrice(bookSellDto.getShippingCost())){
            errors.rejectValue("shippingCost", "Pattern.shippingCost");
        }
    }
}
