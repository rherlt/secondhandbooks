package de.htw.secondhandbooks.validation;

import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.payments.PayPalAccounts;
import de.htw.secondhandbooks.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Objects;

@Component
public class UserDataValidator implements Validator {

    @Autowired
    EmailValidator emailValidator;

    @Autowired
    UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return UserDataContainable.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserDataContainable userData = (UserDataContainable) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "NotEmpty.surname");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty.email");
        if (userData.getPassword() != null) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.password");
        }

        if(!emailValidator.validateEmail(userData.getEmail())){
            errors.rejectValue("email", "Pattern.email");
        }

        if(emailIsUsed(userData)) {
            errors.rejectValue("email", "Duplicate.email");
        }

        if (!userData.getPayPalEmail().equals("")) {
        	if (!emailValidator.validateEmail(userData.getPayPalEmail())){
        		errors.rejectValue("payPalEmail", "Pattern.email");
        	}
        	if (!PayPalAccounts.payPalEmailExists(userData.getPayPalEmail())) {
        		errors.rejectValue("payPalEmail", "Existence.payPalEmail");
        	}
        	if (payPalEmailIsUsed(userData)) {
        		errors.rejectValue("payPalEmail", "Duplicate.payPalEmail");
        	}
    	}

        if (nicknameIsUsed(userData)) {
            errors.rejectValue("nickname", "Duplicate.nickname");
        }

        if (userData.getPassword() != null && !userData.getPassword().equals(userData.getRepeatedPassword())) {
            errors.rejectValue("repeatedPassword", "Mismatch.password");
        }
    }

    private boolean emailIsUsed(UserDataContainable userData) {
        User userWithGivenEmail = userService.findUserByEmail(userData.getEmail());
        return null != userWithGivenEmail && !Objects.equals(userWithGivenEmail.getId(), userData.getId());
    }

    private boolean payPalEmailIsUsed(UserDataContainable userData) {
        User userWithGivenPayPalEmail = userService.findUserByPayPalEmail(userData.getPayPalEmail());
        return null != userWithGivenPayPalEmail && !Objects.equals(userWithGivenPayPalEmail.getId(), userData.getId());
    }

    private boolean nicknameIsUsed(UserDataContainable userData) {
        String nickname = userData.getNickname();
        if (null == nickname || nickname.isEmpty()) {
            return false;
        } else {
            User userWithSameRealname = userService.findUserByRealname(nickname);
            if (null != userWithSameRealname && !Objects.equals(userWithSameRealname.getId(), userData.getId())) {
                return true;
            }
            User userWithSameNickname = userService.findUserByNickname(nickname);
            return null != userWithSameNickname && !Objects.equals(userWithSameNickname.getId(), userData.getId());
        }
    }
}
