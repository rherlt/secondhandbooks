package de.htw.secondhandbooks.validation;

import de.htw.secondhandbooks.dto.BookBuyDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class BookBuyingValidator implements Validator {

    @Autowired
    PriceValidator priceValidator;

    @Override
    public boolean supports(Class<?> clazz) {
        return BookBuyDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        BookBuyDTO bookBuyDto = (BookBuyDTO) target;
        bookBuyDto.setOfferedPrice(bookBuyDto.getOfferedPrice().replace(',', '.').trim());

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "offeredPrice", "NotEmpty.offeredPrice");

        if(!priceValidator.validatePrice(bookBuyDto.getOfferedPrice())){
            errors.rejectValue("offeredPrice", "Pattern.price");
        }

        try {
            double offeredPrice = Double.parseDouble(bookBuyDto.getOfferedPrice());
            if (offeredPrice >= bookBuyDto.getPrice()) {
                errors.rejectValue("offeredPrice", "Invalid.offeredPriceIsHigherThanPrice");
            }

            if (offeredPrice <= bookBuyDto.getHighestOffer()) {
                errors.rejectValue("offeredPrice", "Invalid.offeredPriceIsLowerThanHighestOffer");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
