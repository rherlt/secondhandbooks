package de.htw.secondhandbooks.validation;

import de.htw.secondhandbooks.dto.BookDTO;
import de.htw.secondhandbooks.dto.RatingDTO;
import org.apache.commons.validator.routines.ISBNValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class RateFormValidation implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return RatingDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        RatingDTO ratingDTO = (RatingDTO) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rating", "NotEmpty.rating");

        if (!isRateNumber(ratingDTO)) {
            errors.rejectValue("rating", "Invalid.rating");
        } else if (!isRateValid(ratingDTO)) {
            errors.rejectValue("rating", "Invalid.rating");
        }
    }

    private boolean isRateNumber(RatingDTO ratingDTO) {
        if (ratingDTO.getRating() == null) {
            return false;
        }
        Pattern pattern = Pattern.compile("[0-9]+([.][0-9]{1,2})?");
        Matcher matcher = pattern.matcher(ratingDTO.getRating().toString());
        return matcher.matches();
    }

    private boolean isRateValid(RatingDTO ratingDTO) {
        Float rating = ratingDTO.getRating();
        boolean isValid = false;

        if (rating != null) {
            isValid = rating >= 0 && rating <= 100;
        }
        return isValid;
    }

}
