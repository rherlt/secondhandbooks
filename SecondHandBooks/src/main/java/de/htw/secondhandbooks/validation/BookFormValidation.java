package de.htw.secondhandbooks.validation;

import de.htw.secondhandbooks.dto.BookDTO;
import org.apache.commons.validator.routines.ISBNValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.imageio.ImageIO;
import java.io.InputStream;

@Component
@PropertySource(value = {"classpath:application.properties"})
public class BookFormValidation implements Validator {

    @Autowired
    Environment environment;

    @Override
    public boolean supports(Class<?> clazz) {
        return BookDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        BookDTO bookDTO = (BookDTO) target;
        Long maxImageSize = Long.valueOf(environment.getProperty("max_image_size"));

        if (bookDTO.getTitle().length() < 1) {
            errors.rejectValue("title", "NotEmpty.bookTitle");
        }
        
        if (!isISBNValid(bookDTO)) {
            errors.rejectValue("isbn", "Invalid.isbn");
        }

        if(!bookDTO.getImage().isEmpty()) {


            try (InputStream input = bookDTO.getImage().getInputStream()) {
                    ImageIO.read(input).toString();
            } catch (Exception e) {
                errors.rejectValue("image", "Image.type");
            }

            if (bookDTO.getImage().getSize() > maxImageSize) {
                String sizeString = String.valueOf(maxImageSize/(1024*1024)) + " mb";
                errors.rejectValue("image", "Image.size", new String[]{sizeString}, null);
            }
        }
    }

    private boolean isISBNValid(BookDTO bookDTO) {
        if (bookDTO.getIsbn() == null || bookDTO.getIsbn().isEmpty()) {
            return true;
        } else {
            ISBNValidator validator = new ISBNValidator();
            return validator.isValid(bookDTO.getIsbn());
        }
    }

}
