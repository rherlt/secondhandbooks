package de.htw.secondhandbooks.validation;

import de.htw.secondhandbooks.dto.PasswordChangeDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class PasswordChangeValidator implements Validator {

	public static final class PasswordChangeValidatorContainer {
		String oldPassword;
		PasswordChangeDTO passwordChangeDto;
		public PasswordChangeValidatorContainer(PasswordChangeDTO passwordChangeDTO, String oldPassword) {
			this.passwordChangeDto = passwordChangeDTO;
			this.oldPassword = oldPassword;
		}
	}
	
    @Autowired
    PasswordEncoder passwordEncoder;
	
    @Override
    public boolean supports(Class<?> clazz) {
        return PasswordChangeDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
    	PasswordChangeValidatorContainer passwordContainer = (PasswordChangeValidatorContainer) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "currentPassword", "NotEmpty.password");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPassword", "NotEmpty.password");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "repeatedNewPassword", "NotEmpty.password");

        if (!passwordContainer.passwordChangeDto.getNewPassword().equals(passwordContainer.passwordChangeDto.getRepeatedNewPassword())) {
            errors.rejectValue("repeatedNewPassword", "Mismatch.password");
        }
        
        if (!passwordEncoder.matches(passwordContainer.passwordChangeDto.getCurrentPassword(), passwordContainer.oldPassword)) {
            errors.rejectValue("currentPassword", "Mismatch.currentPassword");
        }
    }
    
}

