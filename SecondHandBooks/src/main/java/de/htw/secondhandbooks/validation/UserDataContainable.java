package de.htw.secondhandbooks.validation;

public interface UserDataContainable {
    Long getId();

    String getName();

    String getSurname();

    String getNickname();

    String getEmail();

    String getPayPalEmail();

    default String getPassword() {
        return null;
    }

    default String getRepeatedPassword() {
        return null;
    }
}
