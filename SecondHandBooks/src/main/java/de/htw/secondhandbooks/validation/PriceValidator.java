package de.htw.secondhandbooks.validation;

import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class PriceValidator implements ConstraintValidator<ValidPrice, String> {
    private static final String PRICE_PATTERN = "[0-9]+([.][0-9]{1,2})?";

    @Override
    public void initialize(ValidPrice constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return (validatePrice(value));
    }

    public boolean validatePrice(String value) {
        Pattern pattern = Pattern.compile(PRICE_PATTERN);
        Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }
}
