package de.htw.secondhandbooks.service;

import de.htw.secondhandbooks.config.PaginationPropertiesConfig;
import de.htw.secondhandbooks.dto.UserReportDTO;
import de.htw.secondhandbooks.model.*;
import de.htw.secondhandbooks.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Service
public class NotificationService {
    @Autowired
    private NotificationRepository repository;

    @Autowired
    UserService userService;

    @Autowired
    EmailService emailService;

    @Autowired
    PayPalPaymentService payPalPaymentService;

    @Autowired
    BookService bookService;

    @Autowired
    PaginationPropertiesConfig paginationPropertiesConfig;

    @Transactional
    public void persistNotification(Notification notification) {
        if (notification != null) {
            repository.save(notification);
        }
    }

    @Transactional
    public void deleteNotification(Notification notificationToDelete) {
        if (notificationToDelete != null) {
            repository.delete(notificationToDelete);
        }
    }

    @Transactional(readOnly = true)
    public List<Notification> findAllNotifications() {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    public Notification findNotificationById(Long id) {
        return repository.findOne(id);
    }


    @Transactional
    public void setNotificationAsRead(Notification notification) {
        notification.setRead(true);
        repository.save(notification);
    }

    /**
     * This method will return all notifications for a specific {@link User}.
     *
     * @param receiver receiver
     * @return List of notifications or empty list
     */
    public List<Notification> getAllUnreadNotificationsForUser(User receiver) {
        return repository.findAllByReceiverAndReadFalse(receiver);
    }

    public Page<Notification> getAllUnreadNotificationsForUser(User receiver, Integer pageNumber) {
        return getAllUnreadNotificationsForUser(receiver, pageNumber, paginationPropertiesConfig.getDefaultItemsPerPage());
    }

    public Page<Notification> getAllUnreadNotificationsForUser(User receiver, Integer pageNumber, Integer pageSize) {
        Sort sort = new Sort(Sort.Direction.DESC, "timeAdded");
        return repository.findAllByReceiverAndReadFalse(receiver, new PageRequest(pageNumber - 1, pageSize, sort));
    }

    public List<Notification> getAllReadNotificationsForUser(User receiver) {
        return repository.findAllByReceiverAndReadTrue(receiver);
    }

    public Page<Notification> getAllReadNotificationsForUser(User receiver, Integer pageNumber) {
        return getAllReadNotificationsForUser(receiver, pageNumber, paginationPropertiesConfig.getDefaultItemsPerPage());
    }

    public Page<Notification> getAllReadNotificationsForUser(User receiver, Integer pageNumber, Integer pageSize) {
        Sort sort = new Sort(Sort.Direction.DESC, "timeAdded");
        return repository.findAllByReceiverAndReadTrue(receiver, new PageRequest(pageNumber - 1, pageSize, sort));
    }

    /**
     * This method will return all notifications which has the principal as receiver.
     * Will return an empty list if user is not logged in.
     *
     * @return List of notifications or empty list
     */
    public List<Notification> getAllUnreadNotificationsForPrincipal(Principal principal) {
        if (principal != null) {
            return getAllUnreadNotificationsForUser(userService.findUserByEmail(principal.getName()));
        }
        return new ArrayList<Notification>();
    }

    public Notification createNotificationForChangeRequest(ChangeRequest changeRequest, HttpServletRequest request) {
        List<User> admins = userService.findAllAdmins();
        String title;
        String message;
        if (changeRequest.isDeletionRequest()) {
            title = "New deletion request";
            message = String.format("User %s wants to delete his/her account." +
                            "<br> <a href=\"%s/admin/deletion-request/%d\">Review this deletion request</a>",
                    changeRequest.getSender().getDisplayName(), getBaseUrl(request), changeRequest.getId());
        } else {
            title = "New change request";
            message = String.format("User %s wants to change his/her profile data." +
                            "<br> <a href=\"%s/admin/change-request/%d\">Review this change request</a>",
                    changeRequest.getSender().getDisplayName(), getBaseUrl(request), changeRequest.getId());
        }
        Notification notification = new Notification(changeRequest.getSender(), admins, title, message);
        sendNotificationEmail(notification);
        return notification;
    }

    public Notification createNotificationForChangeRequestApproval(User sender, User receiver, HttpServletRequest request) {
        String title = "Your profile data has changed";
        String message = "Your request to change your profile data has been accepted. <br> <a href=\"" + getBaseUrl(request) + "/edit-profile\">View your profile</a>";
        return this.createNotificationForChangeRequestBase(sender, receiver, message, title);
    }

    public Notification createNotificationForChangeRequestDenial(User sender, User receiver, String reason, HttpServletRequest request) {
        String title = "Your change request has been declined";
        String message = "Your request to change your profile data has been declined."
                + "<br> Reason: " + reason
                + " <br> <a href=\"" + getBaseUrl(request) + "/edit-profile\">Send another request</a>";
        return this.createNotificationForChangeRequestBase(sender, receiver, message, title);
    }

    public Notification createNotificationForDeletionRequestDenial(User sender, User receiver, String reason, HttpServletRequest request) {
        String title = "Your deletion request has been declined";
        String message = "Your request to delete your account has been declined."
                + "<br> Reason: " + reason
                + " <br> <a href=\"" + getBaseUrl(request) + "/edit-profile\">Send another request</a>";
        return this.createNotificationForChangeRequestBase(sender, receiver, message, title);
    }

    private Notification createNotificationForChangeRequestBase(User sender, User receiver, String message, String title) {
        Notification notification = new Notification(sender, receiver, title, message);
        sendNotificationEmail(notification);
        return notification;
    }

    public Notification createNotificationForPayment(User sender, User receiver, Book book, HttpServletRequest request) {
        String paymentId = payPalPaymentService.createPayment(sender, book, receiver).getId();
        book.setPaymentId(paymentId);
        bookService.updateBook(book);
        String title = "Payment information";
        String message = "Your offer has been accepted."
                +"<br>Please pay the book \"" + book.getTitle() + "\" via the following link:"
                + " <a href=\"" + getBaseUrl(request) + "/pay-book/" + paymentId + "\">payment link</a>.";
        Notification notification = new Notification(sender, receiver, title, message);
        sendNotificationEmail(notification);
        return notification;
    }

    public Notification createNotificationForBookReceived(User sender, User receiver, Book book, HttpServletRequest request) {
        String title = "Book received";
        String message = "You received your bought book from " + sender.getDisplayName()
                + " <br> <a href=\"" + getBaseUrl(request) + "/edit/" + book.getId() + "\">" + book.getTitle() + "</a>";
        Notification notification = new Notification(sender, receiver, title, message);
        sendNotificationEmail(notification);
        return notification;
    }
    
    public Notification createNotificationForOfferMade(User sender, User receiver, Book book, HttpServletRequest request) {
        String title = "Offer made";
        String message =  sender.getDisplayName() + " made an offer for your book "
                + " <br> <a href=\"" + getBaseUrl(request) + "/sell-book/" + book.getId() + "\">" + book.getTitle() + "</a>";
        Notification notification = new Notification(sender, receiver, title, message);
        sendNotificationEmail(notification);
        return notification;
    }
    
    public Notification createNotificationForOfferCanceled(User sender, User receiver, Book book, HttpServletRequest request) {
        String title = "Offer canceled";
        String message =  sender.getDisplayName() + " canceled his/her offer for your book "
                + " <br> <a href=\"" + getBaseUrl(request) + "/sell-book/" + book.getId() + "\">" + book.getTitle() + "</a>";
        Notification notification = new Notification(sender, receiver, title, message);
        sendNotificationEmail(notification);
        return notification;
    }
    
    public Notification createNotificationForOfferOutbid(User sender, User receiver, Book book, HttpServletRequest request) {
        String title = "Your offer got outbidden";

        String message =  sender.getDisplayName() + " has outbid your offer for "
        			+ " <br> " + book.getTitle() + "";
        
        Notification notification = new Notification(sender, receiver, title, message);
        sendNotificationEmail(notification);
        return notification;
    }

    public Notification createNotificationForRatingRequest(User sender, User receiver, HttpServletRequest request) {
        String title = "Rate the seller";
        String message = "You just bough the first time a book from " + sender
                .getDisplayName() + ". It would be " +
                "great if you could rate this seller to share your experience.<br> Please have a look at <a href=\"" +
                getBaseUrl(
                request) + "/rate/" + sender.getId() + "\">this link</a> to submit a rating.";

        Notification notification = new Notification(sender, receiver, title, message);
        sendNotificationEmail(notification);
        return notification;
    }

    public Notification createNotificationForRatingUpdateRequest(User sender, User receiver, HttpServletRequest
            request) {
        String title = "Update your seller rating";
        String message = "You just bough a book from " + sender
                .getDisplayName() + ". You already rated this user. If you want to update your rating, you can do this via this page: " +
                "<a href=\"" + getBaseUrl(
                request) + "/rate/" + sender.getId() + "\"> Rate user</a>.";

        Notification notification = new Notification(sender, receiver, title, message);
        sendNotificationEmail(notification);
        return notification;
    }

    private String getBaseUrl(HttpServletRequest request) {
        // just suspend the port on http or https ports and trust getScheme
        String serverPort = "";
        if (!(request.getServerPort() == 80 || request.getServerPort() == 443)) {
            serverPort = ":" + request.getServerPort();
        }

        return request.getScheme() + "://" + request.getServerName() + serverPort + request.getContextPath();
    }

    private void sendNotificationEmail(Notification notification) {
    	for (int i = 0; i < notification.getReceivers().size(); i++) {
    		User receiver = notification.getReceivers().get(i);
			if (receiver.getNotificationsViaEmail()) {
				String subject = "New notification on SecondHandBooks";
				String message = "You received a new notification on SecondHandBooks:<br>"
								+ notification.getMessage();
				emailService.sendEmail(receiver.getEmail(), message, subject, true);
			}
		}
    }

    public Notification createNotificationForShowcaseReport(User sender, UserReportDTO userToReportDTO, HttpServletRequest request) {
    	if (userToReportDTO.getReason().length() > 500)
    		userToReportDTO.setReason(userToReportDTO.getReason().substring(0, 500));
    	List<User> admins = userService.findAllAdmins();
        User reportedUser = userService.findUserById(userToReportDTO.getId());
        String baseUrl = getBaseUrl(request);
        String title = "New showcase report";
        String message = String.format("User %s reported %s's showcase." +
                            "<br> Reason: %s" +
                            "<br> <a href=\"%s/showcaseoverview/%d\">See showcase</a>" +
                            "<br> <a href=\"%s/admin/user/%d/edit\">Edit reported profile</a>",
                    sender.getDisplayName(), reportedUser.getDisplayName(), userToReportDTO.getReason(), baseUrl, reportedUser.getId(), baseUrl, reportedUser.getId());

        Notification notification = new Notification(sender, admins, title, message);
        sendNotificationEmail(notification);
        return notification;
    }
    
}
