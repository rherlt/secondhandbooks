package de.htw.secondhandbooks.service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

	@Autowired
	private JavaMailSender mailSender;

	public void sendEmail(String recipientAddress, String text, String subject, boolean asHtml) {
		if (asHtml) {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			try {
				mimeMessage.setContent(text, "text/html");
				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, false, "utf-8");
				mimeMessageHelper.setTo(recipientAddress);
				mimeMessageHelper.setSubject(subject);
				mailSender.send(mimeMessage);
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		} else {
			SimpleMailMessage email = new SimpleMailMessage();
			email.setTo(recipientAddress);
			email.setSubject(subject);
			email.setText(text);
			mailSender.send(email);
		}
	}

}
