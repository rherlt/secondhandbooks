package de.htw.secondhandbooks.service;

import de.htw.secondhandbooks.config.PaginationPropertiesConfig;
import de.htw.secondhandbooks.model.Rating;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.repository.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class RatingService {

    @Autowired
    private RatingRepository repository;

    @Autowired
    private UserService userService;

    @Autowired
    private PaginationPropertiesConfig paginationPropertiesConfig;

    @Transactional
    public void createOrUpdateRating(Rating rating) {
        Rating existingRating = findOneByRaterAndSeller(rating.getRater(),rating.getSeller());
        if (existingRating == null) {
            repository.save(rating);
        }
        else {
            existingRating.setDate(rating.getDate());
            existingRating.setRating(rating.getRating());
            existingRating.setComment(rating.getComment());
        }
        User userToUpdate = userService.findUserById(rating.getSeller().getId());
        userToUpdate.setRating(calcAvgRatingForSeller(userToUpdate));
        repository.flush();
    }

    @Transactional
    public void deleteRating(Long id) {
        Rating ratingToDelete = repository.findOneById(id);

        repository.delete(ratingToDelete);
    }

    @Transactional(readOnly = true)
    public List<Rating> findAllRating() {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    public Rating findRatingById(Long id) {
        return repository.findOne(id);
    }

    @Transactional(readOnly = true)
    public Rating findOneByRaterAndSeller(User rater, User seller) {
        return repository.findOneByRaterAndSeller(rater, seller);
    }

    List<Rating> findAllByRaterOrSeller(User rater, User seller) {
        return repository.findAllByRaterOrSeller(rater,seller);
    }

    public List<Rating> findAllBySeller(User seller){
        return repository.findAllBySeller(seller);
    }

    public Page<Rating> findAllBySeller(User seller, Integer pageNumber) {
        return findAllBySeller(seller, pageNumber, paginationPropertiesConfig.getDefaultItemsPerPage());
    }

    public Page<Rating> findAllBySeller(User seller, Integer pageNumber, Integer pageSize) {
        return repository.findAllBySellerOrderByDateDesc(seller, new PageRequest(pageNumber - 1 , pageSize));
    }

    /**
     * Returns the avg of all ratings for this client.
     * Will return -1 if no rating is stored in db yet.
     *
     * @param showcaseOwner
     * @return
     */
    public int calcAvgRatingForSeller(User showcaseOwner) {
        List<Rating> ratings = findAllBySeller(showcaseOwner);

        float rating = -1;
        if (!ratings.isEmpty()) {
            float totalRate = 0;
            int size = ratings.size();
            for (int i = 0; i < size; i++) {
                totalRate += ratings.get(i).getRating();
            }
            rating = totalRate/size;
        }

        return Math.round(rating);
    }
}
