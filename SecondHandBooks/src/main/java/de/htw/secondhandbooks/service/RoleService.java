package de.htw.secondhandbooks.service;

import de.htw.secondhandbooks.model.Role;
import de.htw.secondhandbooks.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleService {

    @Autowired
    RoleRepository repository;

    @Transactional(readOnly = true)
    public Role findRoleByRole(String role) {
        return repository.findByRole(role).get(0);
    }
}
