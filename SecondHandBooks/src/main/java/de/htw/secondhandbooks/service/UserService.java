package de.htw.secondhandbooks.service;

import de.htw.secondhandbooks.config.PaginationPropertiesConfig;
import de.htw.secondhandbooks.dto.UserChangeRequestDTO;
import de.htw.secondhandbooks.dto.UserDTO;
import de.htw.secondhandbooks.filter.UserFilter;
import de.htw.secondhandbooks.filter.UserSortField;
import de.htw.secondhandbooks.filter.UserSortFilter;
import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.Rating;
import de.htw.secondhandbooks.model.Role;
import de.htw.secondhandbooks.model.RoleType;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.model.VerificationToken;
import de.htw.secondhandbooks.repository.UserRepository;
import de.htw.secondhandbooks.repository.VerificationTokenRepository;
import de.htw.secondhandbooks.specifications.UserSpecifications;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class UserService {

    @Autowired
    BookService bookService;

    @Autowired
    RatingService ratingService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    @Autowired
    private UserRepository repository;

    @Autowired
    private VerificationTokenRepository tokenRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private PaginationPropertiesConfig paginationPropertiesConfig;

    @Transactional
    public void createOrUpdateUser(User user) {
        if (user != null) {
            repository.save(user);
        }
    }

    @Transactional
    public void deleteUser(Long id) {
        User userToDelete = repository.findOne(id);
        VerificationToken verificationToken = verificationTokenRepository.findByUser(userToDelete);

        if (verificationToken != null) {
            verificationTokenRepository.delete(verificationToken);
        }

        List<Book> offers = userToDelete.getOffers();
        for (Book book : offers) {
            book.setHighestOffer(0);
            book.setHighestOfferUser(null);
            if (book.isBought()) {
                book.setBought(false);
            }
        }

        List<Rating> ratings = ratingService.findAllByRaterOrSeller(userToDelete,userToDelete);
        for (Rating rating : ratings)  {
            ratingService.deleteRating(rating.getId());
        }

        repository.delete(userToDelete);
    }


    public void saveRegisteredUser(final User user) {
        repository.save(user);
    }

    @Transactional(readOnly = true)
    public List<User> findAllUsers() {
        return repository.findAll();
    }

	@Transactional
	public Page<User> findAllUsers(Integer pageNumber) {
		return repository.findAll(new PageRequest(pageNumber - 1 , paginationPropertiesConfig.getDefaultItemsPerPage()));
	}

	@Transactional(readOnly = true)
	public User findUserById(Long id) {
		return repository.findOne(id);
	}

    @Transactional(readOnly = true)
    public User findUserByEmail(String email) {
        return repository.findByEmail(email);
    }

    @Transactional(readOnly = true)
    public User findUserByPayPalEmail(String payPalEmail) {
        return repository.findByPayPalEmail(payPalEmail);
    }

    @Transactional(readOnly = true)
    public User findUserByNickname(String nickname) {
        return repository.findByNickname(nickname);
    }

    @Transactional(readOnly = true)
    public User findUserByRealname(String realName) {
        return repository.findUserByRealName(realName);
    }

    @Transactional(readOnly = true)
    public float getRatingForUser(User user) {
        float rating = ratingService.calcAvgRatingForSeller(user);
        return rating;
    }

    @Transactional()
    public void addBooksToUser(User user, Collection<Book> books) {
        user.addBooks(books);
        repository.flush();
    }

    @Transactional
    public void updateUser(UserChangeRequestDTO userChangeRequestDTO) {
        if (userChangeRequestDTO != null) {
            User userToUpdate = repository.findOne(userChangeRequestDTO.getId());

            if (userToUpdate != null) {
                userToUpdate.update(userChangeRequestDTO);
                repository.flush();
            }
        }
    }

    @Transactional
    public void updateUser(UserDTO userDTO) {
        if (userDTO != null) {
            User userToUpdate = repository.findOne(userDTO.getId());

			if (userToUpdate != null) {
				if (userDTO.isShowcaseLocked() && !userToUpdate.getShowcaseLocked()) {
					// remove all books on sale
					for (Book book: userToUpdate.getBooksOnSale()) {
						book.setHighestOffer(Double.valueOf(0));
						User highestOfferUser = book.getHighestOfferUser();
						if (highestOfferUser != null) {
							highestOfferUser.removeOffer(book);
						}
						book.setHighestOfferUser(null);
						bookService.updateBook(book);
						this.updateUser(highestOfferUser);

					}
				}

                if (userDTO.isChangedRole()) {
                    Set<Role> roles = new HashSet<>();
                    if (userDTO.isAdmin()) {
                        roles.add(roleService.findRoleByRole("ADMIN"));
                    } else {
                        roles.add(roleService.findRoleByRole("USER"));
                    }

                    userDTO.setRoles(roles);
                    userDTO.setChangedRole(false);
                }

                userToUpdate.update(userDTO);
                repository.flush();
            }
        }
    }

    @Transactional
    public void updateUser(User user) {
        if (user != null) {
            User userToUpdate = repository.findOne(user.getId());

            if (userToUpdate != null) {
                userToUpdate.update(user);
                repository.flush();
            }
        }
    }

    @Transactional
    public User registerNewUserAccount(UserDTO userDTO) {
        String password = passwordEncoder.encode(userDTO.getPassword());
        User user = new User(userDTO.getName(), userDTO.getSurname(), userDTO.getNickname(), userDTO.getEmail(),
                userDTO.getPayPalEmail(), password);
        Role role = roleService.findRoleByRole(RoleType.USER.getRoleType());
        HashSet<Role> roles = new HashSet<>();
        roles.add(role);
        user.setRoles(roles);
        return repository.save(user);
    }

    @Transactional
    public VerificationToken getVerificationToken(final String VerificationToken) {
        return tokenRepository.findByToken(VerificationToken);
    }

    @Transactional
    public void createVerificationTokenForUser(final User user, final String token) {
        final VerificationToken myToken = new VerificationToken(token, user);
        tokenRepository.save(myToken);
    }

    @Transactional
    //TODO get admins from database, not from list with all users
    public List<UserDTO> findAllUsersWithTheirRoles() {
        List<User> users = findAllUsers();

		List<UserDTO> listOfUsersDTO = new ArrayList<>();
		for (User user : users) {
			UserDTO userDTO;
			userDTO = new UserDTO(user, user.getRoles().contains(roleService.findRoleByRole("ADMIN")), user.getEnabled(), user.getShowcaseLocked(), user.getNotificationsViaEmail());
			listOfUsersDTO.add(userDTO);
		}
		return listOfUsersDTO;
	}

    @Transactional
    public Page<UserDTO> findAllUsersWithTheirRolesPageable(Integer pageNumber) {
        return findAllUsersWithTheirRolesPageable(pageNumber, paginationPropertiesConfig.getDefaultItemsPerPage());
    }

	@Transactional
	public Page<UserDTO> findAllUsersWithTheirRolesPageable(Integer pageNumber, Integer pageSize) {
		Page<User> users = this.findAllUsers(pageNumber);

		List<UserDTO> listOfUsersDTO = new ArrayList<>();
		for (User user : users.getContent()) {
			UserDTO userDTO;
			userDTO = new UserDTO(user, user.getRoles().contains(roleService.findRoleByRole("ADMIN")), user.getEnabled(), user.getShowcaseLocked(), user.getNotificationsViaEmail());
			listOfUsersDTO.add(userDTO);
		}
		return new PageImpl<>(
                listOfUsersDTO,
                new PageRequest(pageNumber - 1, pageSize),
                users.getTotalElements()
        );
	}

	@Transactional(readOnly = true)
	public List<User> findAllUsersWithShowcase() {
		return repository.findAllWithShowcase();
	}

    @Transactional(readOnly = true)
    public Page<User> findAllUsersWithShowcase(Integer pageNumber) {
        return repository.findAllWithShowcase(new PageRequest(pageNumber - 1 , paginationPropertiesConfig.getDefaultItemsPerPage()));
    }

    @Transactional(readOnly = true)
    public Page<User> findAllUsersWithShowcaseOrderByShowcaseDateTimeAsc(Integer pageNumber) {
        return repository.findAllWithShowcaseOrderByShowcaseDateTimeAsc(new PageRequest(pageNumber - 1, paginationPropertiesConfig.getDefaultItemsPerPage()));
    }

    @Transactional(readOnly = true)
    public Page<User> findAllUsersWithShowcaseOrderByShowcaseDateTimeDesc(Integer pageNumber) {
        return repository.findAllWithShowcaseOrderByShowcaseDateTimeDesc(new PageRequest(pageNumber - 1, paginationPropertiesConfig.getDefaultItemsPerPage()));
    }

    public Page<User> findAllUsersWithShowcase(Integer pageNumber, UserFilter userFilter, UserSortFilter userSortFilter) {
        return findAllUsersWithShowcase(pageNumber, paginationPropertiesConfig.getDefaultItemsPerPage(), userFilter, userSortFilter);
    }

    @Transactional(readOnly = true)
    public Page<User> findAllUsersWithShowcase(Integer pageNumber, Integer pageSize, UserFilter userFilter, UserSortFilter userSortFilter) {
        PageRequest pageRequest;

        if (userSortFilter.getSortBy().equals(UserSortField.SELLER)) {
            Sort sortFilter = new Sort(Sort.Direction.ASC, "nickname", "name", "surname");
            pageRequest = new PageRequest(pageNumber - 1 , pageSize, sortFilter);
        } else if (userSortFilter.getSortBy().equals(UserSortField.DATE)) {
            Sort sortFilter = new Sort(Sort.Direction.DESC, "latestShowcaseDateTime");
            pageRequest = new PageRequest(pageNumber - 1 , pageSize, sortFilter);
        } else {
            pageRequest = new PageRequest(pageNumber - 1 , pageSize);
        }

        Specification<User> userSpecification = UserSpecifications.showcaseIsNotLocked();
        Specifications<User> userSpecifications = Specifications.where(userSpecification).and(UserSpecifications.hasBooks());

        if (userFilter.getSearchTerm() != null) {
            userSpecifications = Specifications.where(userSpecifications).and(UserSpecifications.findByOwner(userFilter.getSearchTerm()));
        }

        return repository.findAll(userSpecifications, pageRequest);
    }

    @Transactional
    //TODO get admins from database, not from list with all users
    public List<User> findAllAdmins() {
        List<User> users = findAllUsers();
        List<User> admins = new ArrayList<>();
        Role adminRole = roleService.findRoleByRole("ADMIN");


        for (User user : users) {
            if (user.getRoles().contains(adminRole)) {
                admins.add(user);
            }
        }

        return admins;
    }
}
