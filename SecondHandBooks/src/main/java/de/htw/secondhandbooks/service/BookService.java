package de.htw.secondhandbooks.service;

import de.htw.secondhandbooks.config.PaginationPropertiesConfig;
import de.htw.secondhandbooks.filter.BookFilter;
import de.htw.secondhandbooks.filter.BookSearchField;
import de.htw.secondhandbooks.filter.BookSortFilter;
import de.htw.secondhandbooks.specifications.BookSpecifications;
import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.repository.BookRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BookService {
	private static final Logger logger = LoggerFactory.getLogger(BookService.class);

	@Autowired
	private BookRepository repository;

	@Autowired
	private PaginationPropertiesConfig paginationPropertiesConfig;

	@Transactional
	public void createBook(Book book) {
		if (book != null) {
			repository.save(book);
		}
	}

	@Transactional
	public void deleteBook(Long id) {
		Book bookToDelete = repository.findOne(id);

		if (bookToDelete != null) {
			User owner = bookToDelete.getOwner();
			bookToDelete.getOwner().removeBook(bookToDelete);
			owner.update(owner);
			repository.delete(bookToDelete);
		}
	}

	@Transactional(readOnly = true)
	public List<Book> findAllBooks() {
		return repository.findAll();
	}

	@Transactional(readOnly = true)
	public List<Book> findAllBooksByOwnerId(Long ownerId) {
		return repository.findAllByOwnerId(ownerId);
	}


	@Transactional(readOnly = true)
	public Page<Book> findAllByOwnerId(Long ownerId, Pageable pageable) {
		return repository.findAllByOwnerId(ownerId, pageable);
	}

	@Transactional(readOnly = true)
	public Page<Book> findAllByOwnerId(Long ownerId, Integer pageNumber) {
		return repository.findAllByOwnerId(ownerId, new PageRequest(pageNumber - 1 , paginationPropertiesConfig.getDefaultItemsPerPage()));
	}

	@Transactional(readOnly = true)
	public Page<Book> findAllByOwnerId(Long ownerId, Integer pageNumber, BookFilter bookFilter, boolean isSearchingBySellerIncluded, BookSortFilter bookSortFilter) {
		return findAllByOwnerId(
				ownerId,
				paginationPropertiesConfig.getDefaultItemsPerPage(),
				bookFilter,
				isSearchingBySellerIncluded,
				bookSortFilter
				);
	}

	@Transactional(readOnly = true)
	public Page<Book> findAllByOwnerId(Long ownerId, Integer pageNumber, Integer pageSize, BookFilter bookFilter, boolean isSearchingBySellerIncluded, BookSortFilter bookSortFilter) {

		Sort sortFilter = new Sort(bookSortFilter.getDirection(), bookSortFilter.getSortByString());

		PageRequest pageRequest = new PageRequest(pageNumber - 1 , pageSize, sortFilter);

		Specification<Book> bookSpecification = BookSpecifications.byOwnerId(ownerId);
		Specifications<Book> specifications = Specifications.where(bookSpecification);

		specifications = getBookSpecifications(bookFilter, specifications, isSearchingBySellerIncluded);

		if (!bookFilter.isIgnoreToSaleBoolean()) {
			if (bookFilter.isToSale()) {
				specifications = Specifications.where(specifications).and(BookSpecifications.isToSale());
			} else {
				specifications = Specifications.where(specifications).and(BookSpecifications.isNotToSale());
			}
		}

		return repository.findAll(specifications, pageRequest);

	}

	@Transactional(readOnly = true)
	public Book findBookById(Long id) {
		return repository.findOne(id);
	}

	@Transactional(readOnly = true)
	public List<Book> findBooksByTitle(String title) {
		return repository.findByTitle(title);
	}

	@Transactional(readOnly = true)
	public List<Book> findByToSaleTrue() {
		return repository.findByToSaleTrue();
	}

	@Transactional(readOnly = true)
	public Page<Book> findByToSaleTrue(Integer pageNumber) {
		return repository.findByToSaleTrue(new PageRequest(pageNumber - 1 , paginationPropertiesConfig.getDefaultItemsPerPage()));
	}

	@Transactional(readOnly = true)
	public Page<Book> findByToSaleTrueOrderByShowcaseDateTimeAsc(Integer pageNumber) {
		return repository.findByToSaleTrueOrderByShowcaseDateTimeAsc(new PageRequest(pageNumber - 1, paginationPropertiesConfig.getDefaultItemsPerPage()));
	}

	@Transactional(readOnly = true)
	public Page<Book> findByToSaleTrueOrderByShowcaseDateTimeDesc(Integer pageNumber) {
		return repository.findByToSaleTrueOrderByShowcaseDateTimeDesc(new PageRequest(pageNumber - 1, paginationPropertiesConfig.getDefaultItemsPerPage()));
	}

	@Transactional(readOnly = true)
	public Page<Book> findByToSaleTrueAndBoughtFalseOrderByShowcaseDateTimeDesc(Integer pageNumber, BookFilter bookFilter, boolean isSearchingBySellerIncluded,  BookSortFilter bookSortFilter) {
		return findByToSaleTrueAndBoughtFalseOrderByShowcaseDateTimeDesc(
				pageNumber,
				paginationPropertiesConfig.getDefaultItemsPerPage(),
				bookFilter,
				isSearchingBySellerIncluded,
				bookSortFilter
		);
	}

	@Transactional(readOnly = true)
	public Page<Book> findByToSaleTrueAndBoughtFalseOrderByShowcaseDateTimeDesc(Integer pageNumber, Integer pageSize, BookFilter bookFilter, boolean isSearchingBySellerIncluded,  BookSortFilter bookSortFilter) {
		Sort sortFilter = new Sort(bookSortFilter.getDirection(), bookSortFilter.getSortByString());

		PageRequest pageRequest = new PageRequest(pageNumber - 1 , pageSize, sortFilter);

		Specification<Book> specificationToSale = BookSpecifications.isToSale();
		Specification<Book> specificationNotBought = BookSpecifications.isNotBought();
		Specifications<Book>specifications = Specifications.where(specificationToSale).and(specificationNotBought);

		specifications = getBookSpecifications(bookFilter, specifications, isSearchingBySellerIncluded);

		return repository.findAll(specifications, pageRequest);
	}

	private Specifications<Book> getBookSpecifications(BookFilter bookFilter,
													   Specifications<Book> bookSpecifications,
													   boolean isSearchingBySeller) {

		if (bookFilter.getSearchTerm() != null && !bookFilter.getSearchTerm().equals("")) {
			if (bookFilter.getSearchField().equals(BookSearchField.AUTHOR)) {
				bookSpecifications = Specifications.where(bookSpecifications).and(BookSpecifications.authorContains(bookFilter.getSearchTerm()));
			} else if (bookFilter.getSearchField().equals(BookSearchField.DESCRIPTION)) {
				bookSpecifications = Specifications.where(bookSpecifications).and(BookSpecifications.descriptionContains(bookFilter.getSearchTerm()));
			} else if (bookFilter.getSearchField().equals(BookSearchField.ISBN)) {
				bookSpecifications = Specifications.where(bookSpecifications).and(BookSpecifications.isbnContains(bookFilter.getSearchTerm()));
			} else if (bookFilter.getSearchField().equals(BookSearchField.TITLE)) {
				bookSpecifications = Specifications.where(bookSpecifications).and(BookSpecifications.titleContains(bookFilter.getSearchTerm()));
			} else if (bookFilter.getSearchField().equals(BookSearchField.SELLER)) {
				bookSpecifications = Specifications.where(bookSpecifications).and(BookSpecifications.sellerContains(bookFilter.getSearchTerm()));
			} else {
				bookSpecifications = Specifications.where(bookSpecifications).and(BookSpecifications.contains(bookFilter.getSearchTerm(), isSearchingBySeller));
			}
		}
		return bookSpecifications;
	}

	@Transactional(readOnly = true)
	public Page<Book> findAllByOwnerIdAndToSaleIsTrueAndBoughtIsFalse(Long ownerId, Integer pageNumber) {
		return repository.findAllByOwnerIdAndToSaleIsTrueAndBoughtIsFalse(ownerId, new PageRequest(pageNumber - 1 , paginationPropertiesConfig.getDefaultItemsPerPage()));
	}

	public Page<Book> findAllByOwnerIdAndToSaleIsTrueAndBoughtIsFalse(Long ownerId, Integer pageNumber, BookFilter bookFilter, boolean isSearchingBySellerIncluded, BookSortFilter bookSortFilter) {
		return findAllByOwnerIdAndToSaleIsTrueAndBoughtIsFalse(
				ownerId,
				pageNumber,
				paginationPropertiesConfig.getDefaultItemsPerPage(),
				bookFilter,
				isSearchingBySellerIncluded,
				bookSortFilter
		);
	}

	@Transactional(readOnly = true)
	public Page<Book> findAllByOwnerIdAndToSaleIsTrueAndBoughtIsFalse(Long ownerId, Integer pageNumber, Integer pageSize, BookFilter bookFilter, boolean isSearchingBySellerIncluded, BookSortFilter bookSortFilter) {
		Sort sortFilter = new Sort(bookSortFilter.getDirection(), bookSortFilter.getSortByString());

		PageRequest pageRequest = new PageRequest(pageNumber - 1 , pageSize, sortFilter);

		Specification<Book> bookSpecification = BookSpecifications.byOwnerId(ownerId);
		Specifications<Book> specifications = Specifications.where(bookSpecification);

		specifications = getBookSpecifications(bookFilter, specifications, isSearchingBySellerIncluded);

		specifications = Specifications.where(specifications).and(BookSpecifications.isToSale()).and(BookSpecifications.isNotBought());

		return repository.findAll(specifications, pageRequest);
	}
	
	@Transactional
	public void updateBook(Book book) {
		if (book != null) {
			Book bookToUpdate = repository.findOne(book.getId());

			if (bookToUpdate != null) {
				bookToUpdate.update(book);
				repository.flush();
			}
		}
	}

}
