package de.htw.secondhandbooks.service;

import de.htw.secondhandbooks.dto.UserChangeRequestDTO;
import de.htw.secondhandbooks.model.ChangeRequest;
import de.htw.secondhandbooks.model.Notification;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.repository.ChangeRequestRepository;
import de.htw.secondhandbooks.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ChangeRequestService {

	@Autowired
	ChangeRequestRepository repository;

	@Autowired
	UserService userService;

    @Autowired
    NotificationRepository notificationRepository;

	@Transactional
	public void persistChangeRequest(ChangeRequest request) {
		if (request != null) {
			repository.save(request);
		}
	}

    @Transactional
    public void deleteChangeRequest(ChangeRequest requestToDelete) {
        if (requestToDelete != null) {

            Notification notification = notificationRepository.findOne(requestToDelete.getNotification().getId());
            if (notification != null) {
                notificationRepository.delete(notification);
            }

            repository.delete(requestToDelete);
        }
    }

	@Transactional(readOnly = true)
	public ChangeRequest findChangeRequestById(Long id) {
		return repository.findOne(id);
	}

	public ChangeRequest createChangeRequest(UserChangeRequestDTO userChangeRequestDTO) {
		User user = userService.findUserById(userChangeRequestDTO.getId());
		return new ChangeRequest(userChangeRequestDTO.getName(), userChangeRequestDTO.getSurname(),
				userChangeRequestDTO.getNickname(), userChangeRequestDTO.getEmail(),
				userChangeRequestDTO.getPayPalEmail(), user);
	}
}
