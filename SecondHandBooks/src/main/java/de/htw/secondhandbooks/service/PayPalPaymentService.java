package de.htw.secondhandbooks.service;

import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.PayPalPayment;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.repository.PayPalPaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class PayPalPaymentService {
    @Autowired
    private PayPalPaymentRepository repository;

    @Transactional
    public void createPayment(PayPalPayment payment) {
        if (payment != null) {
            repository.save(payment);
            repository.flush();
        }
    }

    @Transactional
    public PayPalPayment createPayment(User payee, Book book, User payer) {
        String id;
        // Use a while loop because we might on accident get the same random String
        do {
            id = UUID.randomUUID().toString();
        } while (findPaymentById(id) != null);
        PayPalPayment payment = new PayPalPayment(id, payee, book, payer);
        repository.save(payment);
        repository.flush();
        return payment;
    }

    @Transactional
    public void updatePayment(PayPalPayment payment) {
        if (payment != null) {
            PayPalPayment paymentToUpdate = repository.findOne(payment.getId());

            if (paymentToUpdate != null) {
                paymentToUpdate.update(payment);
                repository.flush();
            }
        }
    }

    @Transactional(readOnly = true)
    public PayPalPayment findPaymentById(String id) {
        return repository.findOne(id);
    }

    @Transactional(readOnly = true)
    public PayPalPayment findPaymentByPayKey(String payKey) {
        return repository.findByPayKey(payKey);
    }
}
