package de.htw.secondhandbooks.filter;

public enum BookSortField {
    TITLE,
    AUTHOR,
    DATE,
    PRICE,
    OFFER,
    ID
}
