package de.htw.secondhandbooks.filter;

public class UserFilter {

    private String searchTerm;
    private String searchPage;

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public String getSearchPage() {
        return searchPage;
    }

    public void setSearchPage(String searchPage) {
        this.searchPage = searchPage;
    }
}
