package de.htw.secondhandbooks.filter;

public enum BookSearchField {
    ALL,
    TITLE,
    AUTHOR,
    DESCRIPTION,
    ISBN,
    SELLER
}