package de.htw.secondhandbooks.filter;

public class BookFilter {

    private String searchTerm;
    private BookSearchField searchField;

    private boolean toSale;
    private boolean ignoreToSaleBoolean;
    private String searchPage;

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public BookSearchField getSearchField() {
        return searchField;
    }

    public void setSearchField(BookSearchField searchField) {
        this.searchField = searchField;
    }

    public boolean isToSale() {
        return toSale;
    }

    public void setToSale(boolean toSale) {
        this.toSale = toSale;
    }

    public boolean isIgnoreToSaleBoolean() {
        return ignoreToSaleBoolean;
    }

    public void setIgnoreToSaleBoolean(boolean ignoreToSaleBoolean) {
        this.ignoreToSaleBoolean = ignoreToSaleBoolean;
    }

    public String getSearchPage() {
        return searchPage;
    }

    public void setSearchPage(String searchPage) {
        this.searchPage = searchPage;
    }
}
