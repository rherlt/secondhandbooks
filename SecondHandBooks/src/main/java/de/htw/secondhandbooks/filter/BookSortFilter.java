package de.htw.secondhandbooks.filter;

import org.springframework.data.domain.Sort;

public class BookSortFilter {
    private Sort.Direction direction;
    private BookSortField sortBy;
    private String sortPage;

    public Sort.Direction getDirection() {
        return direction;
    }

    public void setDirection(Sort.Direction direction) {
        this.direction = direction;
    }

    public BookSortField getSortBy() {
        return sortBy;
    }

    public void setSortBy(BookSortField sortBy) {
        this.sortBy = sortBy;
    }

    // uhh first string to enum, than back tu sting
    public String getSortByString() {
        if (sortBy != null) {
            if (sortBy.equals(BookSortField.DATE)) {
                return "showcaseDateTime";
            }

            if (sortBy.equals(BookSortField.TITLE)) {
                return "title";
            }

            if (sortBy.equals(BookSortField.PRICE)) {
                return "price";
            }

            if (sortBy.equals(BookSortField.AUTHOR)) {
                return "author";
            }

            if (sortBy.equals(BookSortField.ID)) {
                return "id";
            }
        }
        return "id";
    }

    public String getDirectionString() {
        if (direction != null) {
            if (direction.equals(Sort.Direction.ASC)) {
                return "asc";
            } else {
                return "desc";
            }
        }
        return "asc";
    }

    public String getSortPage() {
        return sortPage;
    }

    public void setSortPage(String sortPage) {
        this.sortPage = sortPage;
    }
}
