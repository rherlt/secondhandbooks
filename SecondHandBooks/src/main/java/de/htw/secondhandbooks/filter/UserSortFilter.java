package de.htw.secondhandbooks.filter;

public class UserSortFilter {
    private UserSortField sortBy;

    public UserSortField getSortBy() {
        return sortBy;
    }

    public void setSortBy(UserSortField sortBy) {
        this.sortBy = sortBy;
    }
}
