package de.htw.secondhandbooks.config;

import de.htw.secondhandbooks.model.RoleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.security.web.session.ConcurrentSessionFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
 
	@Autowired
    LoginSuccessHandler loginSuccessHandler;
	
	@Autowired
	@Qualifier("customUserDetailsService")
	UserDetailsService userDetailsService;
	
    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
    	auth.userDetailsService(userDetailsService);
    }

	@Autowired
	SessionAuthenticationStrategy sessionAuthenticationStrategy;

	@Autowired
	ConcurrentSessionFilter concurrencyFilter;

	@Autowired
	AbstractAuthenticationProcessingFilter authFilter;

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
		authenticationProvider.setUserDetailsService(userDetailsService);
		authenticationProvider.setPasswordEncoder(passwordEncoder());
		return authenticationProvider;
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder builder) throws Exception {
		builder.authenticationProvider(authenticationProvider());
	}

    @Override
    protected void configure(HttpSecurity http) throws Exception {
		http
		.authorizeRequests()
			.antMatchers(
					"/",
					"/login",
					"/registration",
					"/successful-registration",
					"/registration-confirm",
					"/forgot-password",
					"/buy-book/**",
					"/sell-book-overview",
					"/sell-book-overview/**",
					"/showcases",
					"/showcases/**",
					"/showcaseoverview/**",
					"/user/**",
					"/webjars/**",
					"/static/**"
			).permitAll()
			.antMatchers("/admin/**").hasRole(RoleType.ADMIN.getRoleType())
			.anyRequest().authenticated()
			.and()
		.formLogin()
			.loginPage("/login")
			.usernameParameter("email")
			.passwordParameter("password")
			.defaultSuccessUrl("/user-profile")
			.successHandler(loginSuccessHandler)
			.and()
		.exceptionHandling()
			.accessDeniedPage("/access-denied")
			.and()
		.csrf()
		.and()
		.sessionManagement().sessionAuthenticationStrategy(sessionAuthenticationStrategy)
		.and()
		.addFilterBefore(concurrencyFilter, ConcurrentSessionFilter.class);
    }
}