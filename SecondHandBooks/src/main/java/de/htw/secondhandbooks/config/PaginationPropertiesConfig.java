package de.htw.secondhandbooks.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = {"classpath:application.properties"})
public class PaginationPropertiesConfig {

    private static final Logger logger = LoggerFactory.getLogger(PaginationPropertiesConfig.class);

    Integer DEFAULT_ITEMS_PER_PAGE = 9;


    @Autowired
    Environment environment;

    public Integer getPaginationItemsPerPage(String property) {
        try {
            return Integer.valueOf(environment.getProperty(property));
        } catch (NumberFormatException ex) {
            logger.error("NumberFormatException on property <" + property + ">: " + ex.getMessage());
            return getDefaultItemsPerPage();
        } catch (NullPointerException ex) {
            logger.error("NullPointerException on property <" + property + ">: " + ex.getMessage());
            return getDefaultItemsPerPage();
        }
    }

    public Integer getDefaultItemsPerPage() {
        try {
            return Integer.valueOf("pagination.items.default");
        }catch (NumberFormatException ex) {
            logger.error("NumberFormatException on property <pagination.items.default>: " + ex.getMessage());
            return DEFAULT_ITEMS_PER_PAGE;
        } catch (NullPointerException ex) {
            logger.error("NullPointerException on property <pagination.items.default>: " + ex.getMessage());
            return DEFAULT_ITEMS_PER_PAGE;
        }
    }
}
