package de.htw.secondhandbooks.specifications;

import de.htw.secondhandbooks.model.Book_;
import de.htw.secondhandbooks.model.User_;
import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.User;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class BookSpecifications {

    public static Specification<Book> byOwnerId(Long id) {
        return new Specification<Book>() {
            @Override
            public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("owner"), id);
            }
        };
    }

    public static Specification<Book> titleContains(String searchTerm) {
        return genericContains("title", searchTerm);
    }

    public static Specification<Book> authorContains(String searchTerm) {
        return genericContains("author", searchTerm);
    }

    public static Specification<Book> descriptionContains(String searchTerm) {
        return genericContains("description", searchTerm);
    }

    public static Specification<Book> isbnContains(String searchTerm) {
        return genericContains("isbn", searchTerm);
    }

    private static Specification<Book> genericContains(String column, String searchTerm) {
        return new Specification<Book>() {
            @Override
            public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.like(root.get(column), "%" + searchTerm + "%");
            }
        };
    }

    public static Specification<Book> contains(String searchTerm) {
        return contains(searchTerm, false);
    }

    public static Specification<Book> contains(String searchTerm, boolean isSearchingBySellerIncluded) {
        return new Specification<Book>() {
            @Override
            public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {

                if (isSearchingBySellerIncluded) {
                    final Join<Book,User> usersBook = root.join(Book_.owner, JoinType.LEFT);
                    return cb.or(
                            cb.like(usersBook.<String>get(User_.name), "%" + searchTerm + "%"),
                            cb.like(usersBook.<String>get(User_.surname), "%" + searchTerm + "%"),
                            cb.like(usersBook.<String>get(User_.nickname), "%" + searchTerm + "%"),
                            cb.like(root.get("title"), "%" + searchTerm + "%"),
                            cb.like(root.get("author"), "%" + searchTerm + "%"),
                            cb.like(root.get("description"), "%" + searchTerm + "%"),
                            cb.like(root.get("isbn"), "%" + searchTerm + "%")
                    );
                } else {
                    return cb.or(
                            cb.like(root.get("title"), "%" + searchTerm + "%"),
                            cb.like(root.get("author"), "%" + searchTerm + "%"),
                            cb.like(root.get("description"), "%" + searchTerm + "%"),
                            cb.like(root.get("isbn"), "%" + searchTerm + "%"));
                }
            }
        };
    }

    public static Specification<Book> isToSale() {
        return new Specification<Book>() {
            @Override
            public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("toSale"), true);
            }
        };
    }

    public static Specification<Book> isNotToSale() {
        return new Specification<Book>() {
            @Override
            public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("toSale"), false);
            }
        };
    }
    
    public static Specification<Book> isBought() {
        return new Specification<Book>() {
            @Override
            public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("bought"), true);
            }
        };
    }

    public static Specification<Book> isNotBought() {
        return new Specification<Book>() {
            @Override
            public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("bought"), false);
            }
        };
    }

    public static Specification<Book> sellerContains(String searchTerm) {
        return new Specification<Book>() {
            @Override
            public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                final Join<Book,User> usersBook = root.join(Book_.owner, JoinType.LEFT);
                return cb.or(
                        cb.like(usersBook.<String>get(User_.name), "%" + searchTerm + "%"),
                        cb.like(usersBook.<String>get(User_.surname), "%" + searchTerm + "%"),
                        cb.like(usersBook.<String>get(User_.nickname), "%" + searchTerm + "%")
                );
            }
        };
    }
}
