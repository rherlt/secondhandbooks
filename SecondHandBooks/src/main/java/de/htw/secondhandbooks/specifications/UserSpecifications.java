package de.htw.secondhandbooks.specifications;

import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.User;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class UserSpecifications {

    public static Specification<User> showcaseIsNotLocked() {
        return (root, query, cb) -> cb.equal(root.get("showcaseLocked"), false);
    }

    public static Specification<User> findByOwner(String searchTerm) {
        return (root, query, cb) -> cb.or(
                cb.like(root.get("name"), "%" + searchTerm + "%"),
                cb.like(root.get("surname"), "%" + searchTerm + "%"),
                cb.like(root.get("nickname"), "%" + searchTerm + "%")
        );
    }

    public static Specification<User> hasBooks() {
        return (userRoot, query, cb) -> {

            final Subquery<User> bookQuery = query.subquery(User.class);
            final Root<Book> bookRoot = bookQuery.from(Book.class);

            bookQuery.where(cb.isTrue(bookRoot.get("toSale")), cb.isFalse(bookRoot.get("bought")));
            bookQuery.select(bookRoot.<User>get("owner"));

            return cb.in(userRoot.get("id")).value(bookQuery);
        };
    }
}
