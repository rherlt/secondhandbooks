package de.htw.secondhandbooks.utils;

import javax.servlet.http.HttpServletRequest;

public final class Urls {
    public static String getBaseUrl(HttpServletRequest request) {
        // just suspend the port on http or https ports and trust getScheme
        String serverPort = "";
        if (!(request.getServerPort() == 80 || request.getServerPort() == 443)) {
            serverPort = ":" + request.getServerPort();
        }

        return request.getScheme() + "://" + request.getServerName() + serverPort + request.getContextPath();
    }
}
