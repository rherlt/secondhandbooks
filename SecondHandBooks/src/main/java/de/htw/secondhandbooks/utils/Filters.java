package de.htw.secondhandbooks.utils;

import de.htw.secondhandbooks.filter.*;
import org.springframework.data.domain.Sort;
import org.springframework.web.context.request.WebRequest;

public class Filters {

    public static void setBookFilter(BookFilter bookFilter, WebRequest webRequest) {

        String searchTerm = webRequest.getParameter("search-term");
        String searchTermFilter = webRequest.getParameter("search-term-filter");
        String onSaleFilter = webRequest.getParameter("on-sale-filter");

        if (bookFilter.getSearchPage().equals(webRequest.getParameter("search-page"))) {
            if (searchTerm != null) {
                bookFilter.setSearchTerm(searchTerm.trim());
            }

            if (searchTermFilter != null) {
                searchTermFilter = searchTermFilter.toLowerCase();
                switch (searchTermFilter) {
                    case "title":
                        bookFilter.setSearchField(BookSearchField.TITLE);
                        break;
                    case "author":
                        bookFilter.setSearchField(BookSearchField.AUTHOR);
                        break;
                    case "description":
                        bookFilter.setSearchField(BookSearchField.DESCRIPTION);
                        break;
                    case "isbn":
                        bookFilter.setSearchField(BookSearchField.ISBN);
                        break;
                    case "seller":
                        bookFilter.setSearchField(BookSearchField.SELLER);
                        break;
                    case "all":
                        bookFilter.setSearchField(BookSearchField.ALL);
                        break;
                }
            }

            if (onSaleFilter != null) {
                onSaleFilter = onSaleFilter.toLowerCase();
                switch (onSaleFilter) {
                    case "none":
                        bookFilter.setIgnoreToSaleBoolean(true);
                        break;
                    case "true":
                        bookFilter.setIgnoreToSaleBoolean(false);
                        bookFilter.setToSale(true);
                        break;
                    case "false":
                        bookFilter.setIgnoreToSaleBoolean(false);
                        bookFilter.setToSale(false);
                        break;
                }
            } else {
                bookFilter.setIgnoreToSaleBoolean(true);
            }
        }

    }

    public static void setUserFilter(UserFilter userFilter, WebRequest webRequest) {
        String searchTerm = webRequest.getParameter("search-term");

        if (userFilter.getSearchPage().equals(webRequest.getParameter("search-page")) && searchTerm != null) {
            userFilter.setSearchTerm(searchTerm.trim());
        }
    }

    public static void setUserSortFilter(UserSortFilter userSortFilter, WebRequest webRequest) {
        String sortBy = webRequest.getParameter("sort-by");

        if (sortBy != null) {
            sortBy.toLowerCase();
            switch (sortBy) {
                case "seller-name":
                    userSortFilter.setSortBy(UserSortField.SELLER);
                    break;
                case "newest-books":
                    userSortFilter.setSortBy(UserSortField.DATE);
                    break;
                default:
                    userSortFilter.setSortBy(UserSortField.SELLER);
                    break;
            }
        }

        // defaults
        if (sortBy == null && userSortFilter.getSortBy() == null) {
            userSortFilter.setSortBy(UserSortField.SELLER);
        }
    }

    public static void setBookSortFilter(BookSortFilter bookSortFilter, WebRequest webRequest) {
        String direction = webRequest.getParameter("sort-direction");
        String sortBy = webRequest.getParameter("sort-by");

        if (direction != null) {
            direction = direction.toLowerCase();
            switch (direction) {
                case "ascending":
                    bookSortFilter.setDirection(Sort.Direction.ASC);
                    break;
                case "descending":
                    bookSortFilter.setDirection(Sort.Direction.DESC);
                    break;
                default:
                    bookSortFilter.setDirection(Sort.Direction.ASC);
                    break;
            }
        }

        if (sortBy != null) {
            sortBy = sortBy.toLowerCase();
            switch (sortBy) {
                case "date":
                    bookSortFilter.setSortBy(BookSortField.DATE);
                    break;
                case "title":
                    bookSortFilter.setSortBy(BookSortField.TITLE);
                    break;
                case "price":
                    bookSortFilter.setSortBy(BookSortField.PRICE);
                    break;
                case "author":
                    bookSortFilter.setSortBy(BookSortField.AUTHOR);
                    break;
                case "offer":
                    bookSortFilter.setSortBy(BookSortField.OFFER);
                    break;
                default:
                    bookSortFilter.setSortBy(BookSortField.ID);
                    break;
            }
        }

        // defaults
        if (direction == null && bookSortFilter.getDirection() == null
                && sortBy == null && bookSortFilter.getSortBy() == null) {
            if (bookSortFilter.getSortPage().equals("sell-book-overview") || bookSortFilter.getSortPage().equals("showcaseoverview")) {
                bookSortFilter.setSortBy(BookSortField.DATE);
                bookSortFilter.setDirection(Sort.Direction.DESC);
            } else {
                bookSortFilter.setSortBy(BookSortField.TITLE);
                bookSortFilter.setDirection(Sort.Direction.ASC);
            }
        }

    }

    public static void resetSortFilter(BookSortFilter bookSortFilter) {
        bookSortFilter.setSortBy(null);
        bookSortFilter.setDirection(null);
    }
}
