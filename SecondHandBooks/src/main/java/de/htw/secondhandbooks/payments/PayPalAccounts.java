package de.htw.secondhandbooks.payments;

import com.paypal.svcs.services.AdaptiveAccountsService;
import com.paypal.svcs.types.aa.AccountIdentifierType;
import com.paypal.svcs.types.aa.GetVerifiedStatusRequest;
import com.paypal.svcs.types.aa.GetVerifiedStatusResponse;
import com.paypal.svcs.types.common.RequestEnvelope;

import java.util.Map;

public final class PayPalAccounts {
    private static final String MATCH_CRITERIA = "NONE";

    public static boolean payPalEmailExists(String payPalEmail) {
        GetVerifiedStatusRequest request = new GetVerifiedStatusRequest();

        AccountIdentifierType accountIdentifier = new AccountIdentifierType();
        accountIdentifier.setEmailAddress(payPalEmail);
        request.setAccountIdentifier(accountIdentifier);

        request.setMatchCriteria(MATCH_CRITERIA);
        request.setRequestEnvelope(new RequestEnvelope());

        Map<String, String> configurationMap = PayPalConfiguration.getConfiguration();
        AdaptiveAccountsService service = new AdaptiveAccountsService(configurationMap);

        GetVerifiedStatusResponse response = null;
        try {
            response = service.getVerifiedStatus(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response != null && response.getAccountStatus() != null;
    }
}
