package de.htw.secondhandbooks.payments;

import java.util.HashMap;
import java.util.Map;

public final class PayPalConfiguration {
    private static final String MODE = "sandbox";

    private static final String USER_NAME = "shb-d_api1.htw-berlin.de";
    private static final String PASSWORD = "F9WYYBTNB4796R5E";
    private static final String SIGNATURE = "AFcWxV21C7fd0v3bYYYRCpSSRl31ATvA20FJs2FlDV0NpycYHaYoGSzb";
    private static final String APP_ID = "APP-80W284485P519543T";

    public static Map<String, String> getConfiguration() {
        Map<String, String> configurationMap = new HashMap<>();

        configurationMap.putAll(getServiceConfiguration());
        configurationMap.putAll(getAPICredentials());

        return configurationMap;
    }

    private static Map<String, String> getServiceConfiguration() {
        Map<String, String> serviceConfigurationMap = new HashMap<>();

        serviceConfigurationMap.put("mode", MODE);

        return serviceConfigurationMap;
    }

    private static Map<String, String> getAPICredentials() {
        Map<String, String> apiCredentialsMap = new HashMap<>();

        apiCredentialsMap.put("acct1.UserName", USER_NAME);
        apiCredentialsMap.put("acct1.Password", PASSWORD);
        apiCredentialsMap.put("acct1.Signature", SIGNATURE);
        apiCredentialsMap.put("acct1.AppId", APP_ID);

        return apiCredentialsMap;
    }
}
