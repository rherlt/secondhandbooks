package de.htw.secondhandbooks.payments;

import com.paypal.svcs.services.AdaptivePaymentsService;
import com.paypal.svcs.types.ap.*;
import com.paypal.svcs.types.common.RequestEnvelope;

import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.utils.Urls;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class PayPalPayments {
    private static final String ERROR_LANGUAGE = "en_US";
    private static final String ACTION_TYPE = "CREATE";
    private static final String CURRENCY_CODE = "EUR";
    private static final String FEES_PAYER = "SENDER";
    private static final String PAY_KEY_DURATION = "P30D";
    private static final String BUSINESS_NAME = "SecondHandBooks";
    private static final String APPROVE_URL = "https://www.sandbox.paypal.com/webscr?cmd=_ap-payment&paykey=";

    public static String createPayment(Book book, HttpServletRequest request) {
        String payeeEmail = book.getOwner().getPayPalEmail();
        String paymentId = book.getPaymentId();
        String cancelUrl = Urls.getBaseUrl(request) + "/cancelled-payment?id=" + paymentId;
        String returnUrl = Urls.getBaseUrl(request) + "/approved-payment?id=" + paymentId;

        PayResponse payResponse = createTransaction(book.getOverallCost(), payeeEmail, cancelUrl, returnUrl);

        if (payResponse != null && payResponse.getPayKey() != null) {
            String payKey = payResponse.getPayKey();

            SetPaymentOptionsResponse setPaymentOptionsResponse = setTransactionDetails(payeeEmail, book.getTitle(),
                    book.getHighestOffer(), book.getShippingCost(), payKey);

            if (setPaymentOptionsResponse != null && setPaymentOptionsResponse.getError().size() == 0) {
                return payKey;
            }
        }

        return null;
    }

    public static String getApproveUrl(String payKey) {
        return APPROVE_URL + payKey;
    }

    public static boolean isPaymentSuccessfull(String payKey) {
        return getTransactionDetails(payKey).getStatus().equalsIgnoreCase("COMPLETED");
    }

    private static PayResponse createTransaction(double amount, String payeeEmail, String cancelUrl, String returnUrl) {
        PayRequest request = new PayRequest();

        List<Receiver> receivers = new ArrayList<>();
        Receiver receiver = new Receiver();
        receiver.setAmount(amount);
        receiver.setEmail(payeeEmail);
        receivers.add(receiver);
        ReceiverList receiverList = new ReceiverList(receivers);
        request.setReceiverList(receiverList);

        request.setRequestEnvelope(createRequestEnvelope());
        request.setActionType(ACTION_TYPE);
        request.setCurrencyCode(CURRENCY_CODE);
        request.setFeesPayer(FEES_PAYER);
        request.setPayKeyDuration(PAY_KEY_DURATION);
        request.setCancelUrl(cancelUrl);
        request.setReturnUrl(returnUrl);

        PayResponse response = null;
        try {
            AdaptivePaymentsService service = createAdaptivePaymentsService();
            response = service.pay(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    private static SetPaymentOptionsResponse setTransactionDetails(String receiverEmail, String name, double price,
                                                                   double shippingCost, String payKey) {
        SetPaymentOptionsRequest request = new SetPaymentOptionsRequest(createRequestEnvelope(), payKey);

        DisplayOptions displayOptions = new DisplayOptions();
        displayOptions.setBusinessName(BUSINESS_NAME);
        request.setDisplayOptions(displayOptions);

        List<ReceiverOptions> receiverOptions = new ArrayList<>();
        ReceiverOptions option = new ReceiverOptions();
        ReceiverIdentifier receiver = new ReceiverIdentifier();
        receiver.setEmail(receiverEmail);
        option.setReceiver(receiver);
        InvoiceData invoiceData = new InvoiceData();
        List<InvoiceItem> invoiceItems = new ArrayList<>();
        InvoiceItem item = new InvoiceItem();
        item.setName(name);
        item.setPrice(price);
        invoiceItems.add(item);
        invoiceData.setItem(invoiceItems);
        invoiceData.setTotalShipping(shippingCost);
        option.setInvoiceData(invoiceData);
        receiverOptions.add(option);
        request.setReceiverOptions(receiverOptions);

        SetPaymentOptionsResponse response = null;
        try {
            AdaptivePaymentsService service = createAdaptivePaymentsService();
            response = service.setPaymentOptions(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    private static PaymentDetailsResponse getTransactionDetails(String payKey) {
        RequestEnvelope requestEnvelope = createRequestEnvelope();
        PaymentDetailsRequest request = new PaymentDetailsRequest(requestEnvelope);
        request.setPayKey(payKey);

        AdaptivePaymentsService service = createAdaptivePaymentsService();
        PaymentDetailsResponse response = null;
        try {
            response = service.paymentDetails(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    private static RequestEnvelope createRequestEnvelope() {
        RequestEnvelope requestEnvelope = new RequestEnvelope();
        requestEnvelope.setErrorLanguage(ERROR_LANGUAGE);

        return requestEnvelope;
    }

    private static AdaptivePaymentsService createAdaptivePaymentsService() {
        Map<String, String> configurationMap = PayPalConfiguration.getConfiguration();

        return new AdaptivePaymentsService(configurationMap);
    }
}
