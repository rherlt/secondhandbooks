package de.htw.secondhandbooks.util;

import de.htw.secondhandbooks.model.Notification;

import java.util.Comparator;

public class NotificationComparator implements Comparator<Notification> {
    @Override
    public int compare(Notification notification, Notification t1) {
        return t1.getTimeAdded().compareTo(notification.getTimeAdded());
    }
}
