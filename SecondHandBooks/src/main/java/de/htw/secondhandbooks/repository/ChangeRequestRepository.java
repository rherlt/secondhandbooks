package de.htw.secondhandbooks.repository;

import de.htw.secondhandbooks.model.ChangeRequest;
import org.springframework.data.repository.Repository;

public interface ChangeRequestRepository extends Repository<ChangeRequest, Long> {
    void flush();

    ChangeRequest save(ChangeRequest persisted);

    ChangeRequest findOne(Long id);

    void delete(ChangeRequest deleted);
}
