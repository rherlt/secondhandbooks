package de.htw.secondhandbooks.repository;

import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.model.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {

    VerificationToken findByToken(String token);

    VerificationToken findByUser(User user);

}
