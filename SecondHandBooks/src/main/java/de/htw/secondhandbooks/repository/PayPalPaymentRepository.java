package de.htw.secondhandbooks.repository;

import de.htw.secondhandbooks.model.PayPalPayment;
import org.springframework.data.repository.Repository;

public interface PayPalPaymentRepository extends Repository<PayPalPayment, String> {
    PayPalPayment save(PayPalPayment payPalPayment);

    PayPalPayment findOne(String id);

    PayPalPayment findByPayKey(String payKey);

    void flush();
}
