package de.htw.secondhandbooks.repository;

import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.Rating;
import de.htw.secondhandbooks.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RatingRepository extends JpaRepository<Rating, Long> {

    void flush();

    void delete(Rating deleted);

    List<Rating> findAll();

    Rating findOneById(Long id);

    List<Rating> findAllByRaterOrSeller(User rater, User seller);

    Rating findOneByRaterAndSeller(User rater, User seller);

    Rating save(Rating persisted);

    List<Rating> findAllBySeller(User seller);

    Page<Rating> findAllBySellerOrderByDateDesc(User seller, Pageable pageable);

}
