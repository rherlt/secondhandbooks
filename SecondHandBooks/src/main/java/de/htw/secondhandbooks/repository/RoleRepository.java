package de.htw.secondhandbooks.repository;

import de.htw.secondhandbooks.model.Role;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface RoleRepository extends Repository<Role, Integer> {

	List<Role> findByRole(String role);
}
