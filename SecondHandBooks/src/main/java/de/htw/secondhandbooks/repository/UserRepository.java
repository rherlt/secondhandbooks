package de.htw.secondhandbooks.repository;

import de.htw.secondhandbooks.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    void flush();

    void delete(User deleted);

    List<User> findAll();

    Page<User> findAll(Pageable pageable);

    Page<User> findAll(Specification<User> specification, Pageable pageable);

    User findByEmail(String email);

    User findByPayPalEmail(String payPalEmail);

    User findByNickname(String nickname);

    User findOne(Long id);

    User save(User persisted);

    @Query("SELECT DISTINCT u FROM User u WHERE CONCAT(u.name, ' ', u.surname) = :realname")
    User findUserByRealName(@Param("realname") String realName);

    @Query("SELECT DISTINCT u FROM User u JOIN u.books as b WHERE b.toSale = TRUE AND u.showcaseLocked = FALSE")
    List<User> findAllWithShowcase();

    @Query("SELECT DISTINCT u FROM User u JOIN u.books as b WHERE b.toSale = TRUE AND u.showcaseLocked = FALSE")
    Page<User> findAllWithShowcase(Pageable pageable);

    @Query("SELECT DISTINCT u FROM User u JOIN u.books as b WHERE b.toSale = TRUE AND u.showcaseLocked = FALSE ORDER BY b.showcaseDateTime ASC")
    Page<User> findAllWithShowcaseOrderByShowcaseDateTimeAsc(Pageable pageable);

    @Query("SELECT DISTINCT u FROM User u JOIN u.books as b WHERE b.toSale = TRUE AND u.showcaseLocked = FALSE ORDER BY b.showcaseDateTime DESC")
    Page<User> findAllWithShowcaseOrderByShowcaseDateTimeDesc(Pageable pageable);
}
