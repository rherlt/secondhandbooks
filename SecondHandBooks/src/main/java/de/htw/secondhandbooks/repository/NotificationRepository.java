package de.htw.secondhandbooks.repository;

import de.htw.secondhandbooks.model.Notification;
import de.htw.secondhandbooks.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NotificationRepository extends Repository<Notification, Long> {

    void flush();

    void delete(Notification deleted);

    List<Notification> findAll();

    Notification findOne(Long id);

    Notification save(Notification persisted);

    @Query("SELECT n FROM Notification n INNER JOIN n.receivers r WHERE r = :receiver")
    List<Notification> findAllByReceiver(@Param("receiver") User receiver);

    @Query("SELECT n FROM Notification n INNER JOIN n.receivers r WHERE r = :receiver AND n.read=false")
    List<Notification> findAllByReceiverAndReadFalse(@Param("receiver") User receiver);

    @Query("SELECT n FROM Notification n INNER JOIN n.receivers r WHERE r = :receiver AND n.read=false")
    Page<Notification> findAllByReceiverAndReadFalse(@Param("receiver") User receiver, Pageable pageable);

    @Query("SELECT n FROM Notification n INNER JOIN n.receivers r WHERE r = :receiver AND n.read=true")
    List<Notification> findAllByReceiverAndReadTrue(@Param("receiver") User receiver);

    @Query("SELECT n FROM Notification n INNER JOIN n.receivers r WHERE r = :receiver AND n.read=true")
    Page<Notification> findAllByReceiverAndReadTrue(@Param("receiver") User receiver, Pageable pageable);
}
