package de.htw.secondhandbooks.repository;

import de.htw.secondhandbooks.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;


public interface BookRepository extends JpaRepository<Book, Long>, JpaSpecificationExecutor<Book> {

	void flush();

	void delete(Book deleted);

	List<Book> findByTitle(String title);

	List<Book> findAll();

	List<Book> findAllByOwnerId(Long ownerId);

	Page<Book> findAllByOwnerId(Long ownerId, Pageable pageable);

	Book findOne(Long id);

	Book save(Book persisted);
	
	List<Book> findByToSaleTrue();

	Page<Book> findByToSaleTrue(Pageable pageable);

	Page<Book> findByToSaleTrueOrderByShowcaseDateTimeAsc(Pageable pageable);

	Page<Book> findByToSaleTrueOrderByShowcaseDateTimeDesc(Pageable pageable);

	Page<Book> findAllByOwnerIdAndToSaleIsTrueAndBoughtIsFalse(Long ownerId, Pageable pageable);

	Page<Book> findAllByOwnerIdAndToSaleIsFalse(Long ownerId, Pageable pageable);

	// specs
	Page<Book> findAll(Specification<Book> specification, Pageable pageable);

	Page<Book> findByToSaleTrueOrderByShowcaseDateTimeDesc(Specification<Book> specification, Pageable pageable);

	Page<Book> findAllByOwnerId(Long ownerId, Specification<Book> specification, Pageable pageable);

}
