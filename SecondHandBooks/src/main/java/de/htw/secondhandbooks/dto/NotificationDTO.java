package de.htw.secondhandbooks.dto;

import de.htw.secondhandbooks.model.Notification;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class NotificationDTO {
    @NotNull
    @NotEmpty
    private Long id;

    @NotNull
    @NotEmpty
    private String title;

    @NotNull
    @NotEmpty
    private String message;

    @NotNull
    @NotEmpty

    private Date timeAdded;

    @NotNull
    @NotEmpty
    private boolean read = false;

    public NotificationDTO () {

    }

    public NotificationDTO(Notification notification) {
        this.id = notification.getId();
        this.title = notification.getTitle();
        this.message = notification.getMessage();
        this.timeAdded = notification.getTimeAdded();
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public Date getTimeAdded() {
        return timeAdded;
    }

    public boolean isRead() {
        return read;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTimeAdded(Date timeAdded) {
        this.timeAdded = timeAdded;
    }

    public void setRead(boolean read) {
        this.read = read;
    }
}
