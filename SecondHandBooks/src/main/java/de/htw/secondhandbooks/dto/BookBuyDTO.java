package de.htw.secondhandbooks.dto;

import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.PayPalPayment;
import de.htw.secondhandbooks.model.User;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class BookBuyDTO {

	@NotNull
	@NotEmpty
	private Long id;

	@NotNull
	@NotEmpty
	private String title;

	@NotNull
	@NotEmpty
	private String author;

	@NotNull
	@NotEmpty
	private String description;

	@NotNull
	@NotEmpty
	private String isbn;
	
	@NotNull
	@NotEmpty
	private User seller;

	private double price;

	private double shippingCost;

	@NotNull
	@NotEmpty
	private String offeredPrice;

	private double highestOffer;

	private boolean highestOfferBySameUser;

	private boolean bought;

	private String paymentId;

	public void setBookData(Book book) {
		this.id = book.getId();
		this.title = book.getTitle();
		this.author = book.getAuthor();
		this.description = book.getDescription();
		this.isbn = book.getIsbn();
		this.price = book.getPrice();
		this.seller = book.getOwner();
		this.shippingCost = book.getShippingCost();
		this.highestOffer = book.getHighestOffer();
		this.bought = book.isBought();
		this.offeredPrice = String.format("%.2f", book.getHighestOffer());
		this.paymentId = book.getPaymentId();
	}

	public void setBuyingData(Book book, User user) {
		if (user != null && book.getHighestOfferUser() != null) {
			if (book.getHighestOfferUser().getId().equals(user.getId())) {
				this.highestOfferBySameUser = true;
			}
		}

	}


	public void updateBookData(Book book, User user) {
		this.id = book.getId();
		this.title = book.getTitle();
		this.author = book.getAuthor();
		this.description = book.getDescription();
		this.isbn = book.getIsbn();
		this.price = book.getPrice();
		this.seller = book.getOwner();
		this.shippingCost = book.getShippingCost();
		this.bought = book.isBought();
		setBuyingData(book, user);
		this.paymentId = book.getPaymentId();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
	
	public User getSeller() {
		return seller;
	}

	public void setSeller(User seller) {
		this.seller = seller;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getShippingCost() {
		return shippingCost;
	}

	public void setShippingCost(double shippingCost) {
		this.shippingCost = shippingCost;
	}

	public String getOfferedPrice() {
		return offeredPrice;
	}

	public void setOfferedPrice(String offeredPrice) {
		this.offeredPrice = offeredPrice;
	}

	public double getHighestOffer() {
		return highestOffer;
	}

	public void setHighestOffer(double highestOffer) {
		this.highestOffer = highestOffer;
	}

	public boolean isHighestOfferBySameUser() {
		return highestOfferBySameUser;
	}

	public void setHighestOfferBySameUser(boolean highestOfferBySameUser) {
		this.highestOfferBySameUser = highestOfferBySameUser;
	}

	public double getOverallCost() {
		if (isBought()) {			
			return getHighestOffer() + getShippingCost();
		}
		return getPrice() + getShippingCost();
	}

	public boolean isBought() {
		return bought;
	}

	public void setBought(boolean bought) {
		this.bought = bought;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
}