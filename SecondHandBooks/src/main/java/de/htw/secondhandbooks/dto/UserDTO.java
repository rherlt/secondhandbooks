package de.htw.secondhandbooks.dto;

import de.htw.secondhandbooks.model.Role;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.validation.UserDataContainable;
import de.htw.secondhandbooks.validation.ValidEmail;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.Set;

public class UserDTO implements UserDataContainable {
    @NotNull
    private Long id;
    @NotNull
    @NotEmpty
    private String name;
    @NotNull
    @NotEmpty
    private String surname;
    private String nickname;
    @NotNull
    @NotEmpty
    @ValidEmail
    private String email;
    @ValidEmail
    private String payPalEmail;
    @NotNull
    @NotEmpty
    private String password;
    @NotNull
    @NotEmpty
    private String repeatedPassword;

    private String reason;

    private boolean admin;

    private boolean enabled;

    private Set<Role> roles;

    private boolean changedRole;

    private boolean showcaseLocked;
    
    private boolean notificationsViaEmail;

    public UserDTO() {
    }

    public UserDTO(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.surname = user.getSurname();
        this.nickname = user.getNickname();
        this.email = user.getEmail();
        this.payPalEmail = user.getPayPalEmail();
        this.password = user.getPassword();
        this.showcaseLocked = user.getShowcaseLocked();
        this.notificationsViaEmail = user.getNotificationsViaEmail();
    }

    public UserDTO(User user, boolean admin, boolean enabled, boolean showcaseLocked, boolean notificationsViaEmail) {
        this.id = user.getId();
        this.name = user.getName();
        this.surname = user.getSurname();
        this.nickname = user.getNickname();
        this.email = user.getEmail();
        this.payPalEmail = user.getPayPalEmail();
        this.password = user.getPassword();
        this.admin = admin;
        this.enabled = enabled;
        this.showcaseLocked = showcaseLocked;
        this.notificationsViaEmail = notificationsViaEmail;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
    	if (name.length() > 50)
    		name = name.substring(0, 50);
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
    	if (surname.length() > 50)
    		surname = surname.substring(0, 50);
        this.surname = surname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
    	if (nickname.length() > 50)
    		nickname = nickname.substring(0, 50);
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPayPalEmail() {
        return payPalEmail;
    }

    public void setPayPalEmail(String payPalEmail) {
        this.payPalEmail = payPalEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatedPassword() {
        return repeatedPassword;
    }

    public void setRepeatedPassword(String repeatedPassword) {
        this.repeatedPassword = repeatedPassword;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public boolean isChangedRole() {
        return changedRole;
    }

    public void setChangedRole(boolean changedRole) {
        this.changedRole = changedRole;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return this.reason;
    }

    public boolean isShowcaseLocked() {
        return showcaseLocked;
    }

    public void setShowcaseLocked(boolean showcaseLocked) {
        this.showcaseLocked = showcaseLocked;
    }

	public boolean isNotificationsViaEmail() {
		return notificationsViaEmail;
	}

	public void setNotificationsViaEmail(boolean notificationsViaEmail) {
		this.notificationsViaEmail = notificationsViaEmail;
	}

}
