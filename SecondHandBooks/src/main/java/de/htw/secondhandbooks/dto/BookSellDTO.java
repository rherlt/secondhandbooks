package de.htw.secondhandbooks.dto;

import de.htw.secondhandbooks.model.Book;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class BookSellDTO {

	@NotNull
	@NotEmpty
	private Long id;

	@NotNull
	@NotEmpty
	private String title;

	@NotNull
	@NotEmpty
	private String author;

	@NotNull
	@NotEmpty
	private String description;

	@NotNull
	@NotEmpty
	private String isbn;

	@NotNull
	@NotEmpty
	private String offeredPrice;

	@NotNull
	@NotEmpty
	private String price;

	@NotNull
	@NotEmpty
	private String shippingCost;

	@NotNull
	@NotEmpty
	private boolean toSale;
	
	@NotNull
	@NotEmpty
	private boolean bought;

	private boolean updateBookSale;

	private boolean validOfferAvailable;

	public void setBookData(Book book) {
		this.id = book.getId();
		this.title = book.getTitle();
		this.description = book.getDescription();
		this.isbn = book.getIsbn();
		this.author=book.getAuthor();
		this.offeredPrice = Double.toString(book.getHighestOffer());
		this.price = String.format("%.2f", book.getPrice());
		this.shippingCost = String.format("%.2f", book.getShippingCost());
		this.toSale = book.isToSale();
		this.bought = book.isBought();
	}

	public void setSellingData(Book book) {
		this.updateBookSale = book.isToSale() && !book.isBought();
		this.validOfferAvailable = book.getHighestOffer() > 0 && book.getHighestOfferUser() != null && !book.isBought();
	}

	public void updateBookData(Book book) {
		this.id = book.getId();
		this.title = book.getTitle();
		this.description = book.getDescription();
		this.isbn = book.getIsbn();
		this.author=book.getAuthor();
		this.offeredPrice = Double.toString(book.getHighestOffer());
		this.toSale = book.isToSale();
		this.bought = book.isBought();
		this.setSellingData(book);
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getShippingCost() {
		return shippingCost;
	}

	public void setShippingCost(String shippingCost) {
		this.shippingCost = shippingCost;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getOfferedPrice() {
		return offeredPrice;
	}

	public void setOfferedPrice(String offeredPrice) {
		this.offeredPrice = offeredPrice;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isToSale() {
		return toSale;
	}

	public void setToSale(boolean toSale) {
		this.toSale = toSale;
	}
	
	public boolean isBought() {
		return bought;
	}

	public void setBought(boolean bought) {
		this.bought = bought;
	}

	public boolean isUpdateBookSale() {
		return updateBookSale;
	}

	public void setUpdateBookSale(boolean updateBookSale) {
		this.updateBookSale = updateBookSale;
	}

	public boolean isValidOfferAvailable() {
		return validOfferAvailable;
	}

	public void setValidOfferAvailable(boolean validOfferAvailable) {
		this.validOfferAvailable = validOfferAvailable;
	}
}
