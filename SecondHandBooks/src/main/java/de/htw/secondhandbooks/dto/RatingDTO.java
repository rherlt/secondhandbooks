package de.htw.secondhandbooks.dto;

import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.User;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

public class RatingDTO {

    private String id;

    private User soldBy;

    private User ratedBy;

    private Float rating;

    private String comment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getSoldBy() {
        return soldBy;
    }

    public void setSoldBy(User soldBy) {
        this.soldBy = soldBy;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
    	if (comment.length() > 500)
    		comment = comment.substring(0, 500);
        this.comment = comment;
    }

    public User getRatedBy() {
        return ratedBy;
    }

    public void setRatedBy(User ratedBy) {
        this.ratedBy = ratedBy;
    }
}
