package de.htw.secondhandbooks.dto;

import de.htw.secondhandbooks.model.ChangeRequest;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.validation.UserDataContainable;
import de.htw.secondhandbooks.validation.ValidEmail;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class UserChangeRequestDTO implements UserDataContainable {
    @NotNull
    private Long id;
    @NotNull
    @NotEmpty
    private String name;
    @NotNull
    @NotEmpty
    private String surname;
    private String nickname;
    @NotNull
    @NotEmpty
    @ValidEmail
    private String email;
    @NotNull
    @NotEmpty
    @ValidEmail
    private String payPalEmail;

    private String reason;

    public UserChangeRequestDTO() {
    }

    public UserChangeRequestDTO(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.surname = user.getSurname();
        this.nickname = user.getNickname();
        this.email = user.getEmail();
        this.payPalEmail = user.getPayPalEmail();
    }

    public UserChangeRequestDTO(ChangeRequest changeRequest) {
        this.id = changeRequest.getSender().getId();
        this.name = changeRequest.getName();
        this.surname = changeRequest.getSurname();
        this.nickname = changeRequest.getNickname();
        this.email = changeRequest.getEmail();
        this.payPalEmail = changeRequest.getPayPalEmail();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
    	if (name.length() > 50)
    		name = name.substring(0, 50);
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
    	if (surname.length() > 50)
    		surname = surname.substring(0, 50);
        this.surname = surname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
    	if (nickname.length() > 50)
    		nickname = nickname.substring(0, 50);
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getPayPalEmail() {
        return payPalEmail;
    }

    public void setPayPalEmail(String payPalEmail) {
        this.payPalEmail = payPalEmail;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
    	if (reason.length() > 500)
    		reason = reason.substring(0, 500);
        this.reason = reason;
    }
}
