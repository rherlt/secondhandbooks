package de.htw.secondhandbooks.dto;


import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

public class NotificationDTOListWrapper {

    @Valid
    private List<NotificationDTO> notificationDTOList = new ArrayList<>();

    public NotificationDTOListWrapper() {
    }

    public List<NotificationDTO> getNotificationDTOList() {
        return notificationDTOList;
    }

    public void setNotificationDTOList(List<NotificationDTO> notificationDTOList) {
        this.notificationDTOList = notificationDTOList;
    }

    public void add(NotificationDTO notificationDTO) {
        this.notificationDTOList.add(notificationDTO);
    }
}
