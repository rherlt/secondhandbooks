package de.htw.secondhandbooks.dto;

import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.User;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

public class BookDTO {

	@NotNull
	@NotEmpty
	private String id;

	@NotNull
	@NotEmpty
	private String title;

	@NotNull
	@NotEmpty
	private String author;

	@NotNull
	@NotEmpty
	private String description;

	@NotNull
	@NotEmpty
	private String isbn;

	@NotNull
	@NotEmpty
	private String bought;

	@NotNull
	@NotEmpty
	private String sale;

	@NotNull
	@NotEmpty
	private MultipartFile image;

	@NotNull
	@NotEmpty
	private User owner;

	private boolean removeImage = false;

	    public String getId() {
	        return id;
	    }

	    public void setId(String id) {
	        this.id = id;
	    }
	    
	    public String getTitle() {
	        return title;
	    }

	    public void setTitle(String title) {
	    	if (title.length() > 50)
	    		title = title.substring(0, 50);
	        this.title = title;
	    }

	    public String getDescription() {
	        return description;
	    }

	    public void setDescription(String description) {
	    	if (description.length() > 500)
	    		description = description.substring(0, 500);
	        this.description = description;
	    }

	    public String getBought() {
	        return bought;
	    }

	    public void setBought(String bought) {
	        this.bought = bought;
	    }

	    public String getSale() {
	        return sale;
	    }

	    public void setSale(String sale) {
	        this.sale = sale;
	    }

	    public MultipartFile getImage() {
	        return image;
	    }

	    public void setImage(MultipartFile image) {
	        this.image = image;
	    }
	    
	    public User getOwner() {
	        return owner;
	    }

	    public void setOwner(User owner) {
	        this.owner = owner;
	    }
	    
		public void setBookData(Book book)
		{
			this.id=book.getId().toString();
			this.title=book.getTitle();
			this.author=book.getAuthor();
			this.description=book.getDescription();
			this.isbn=book.getIsbn();
			this.owner=book.getOwner();
			if(book.isBought())
				this.bought="yes";
			else
				this.bought="no";
			if(book.isToSale())
				this.sale="yes";
			else
				this.sale="no";
		}

		public String getAuthor() {
			return author;
		}

		public void setAuthor(String author) {
			if (author.length() > 50)
				author = author.substring(0, 50);
			this.author = author;
		}

		public String getIsbn() {
			return isbn;
		}

		public void setIsbn(String isbn) {
			this.isbn = isbn;
		}

		public boolean isRemoveImage() {
			return removeImage;
		}

		public void setRemoveImage(boolean removeImage) {
			this.removeImage = removeImage;
		}
}
