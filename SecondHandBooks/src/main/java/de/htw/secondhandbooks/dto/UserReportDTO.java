package de.htw.secondhandbooks.dto;

import de.htw.secondhandbooks.model.User;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class UserReportDTO {
    @NotNull
    private Long id;
    @NotNull
    @NotEmpty
    private String reason;

    public UserReportDTO() {
    }

    public UserReportDTO(User user) {
        this.id = user.getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setReason(String reason) {
    	if (reason.length() > 500)
    		reason = reason.substring(0, 500);
        this.reason = reason;
    }

    public String getReason() {
        return this.reason;
    }

}
