package de.htw.secondhandbooks.integrationtest;

import static org.junit.Assert.assertTrue;

import de.htw.secondhandbooks.config.SecurityConfigurationTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import de.htw.secondhandbooks.config.PersistenceContextTest;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { PersistenceContextTest.class, SecurityConfigurationTest.class})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:UserTestData.xml")
public class UserIntegrationTest {

	@Autowired
	private UserService service;
	
	@Test
	public void persistNewUserTest() {
		int userCountBeforeInsertion = service.findAllUsers().size();
		User user = new User("Mustermann", "Max", "Maxi", "max_mustermann@gmail.com", "max_mustermann@gmail.com",
				"pwd123");
		service.createOrUpdateUser(user);
		
		boolean userCountIncreasedByOne = service.findAllUsers().size() == userCountBeforeInsertion + 1;
		boolean newUserExists = service.findUserById(user.getId()) != null;
		
		assertTrue(userCountIncreasedByOne && newUserExists);
	}
	
	@Test
	public void deleteUserTest() {
		Long userID = service.findAllUsers().get(0).getId();
		int userCountBeforeDeletion = service.findAllUsers().size();
		service.deleteUser(userID);
		
		boolean userCountDecreasedByOne = service.findAllUsers().size() == userCountBeforeDeletion - 1;
		boolean userGotRemoved = service.findUserById(userID) == null;
		
		assertTrue(userCountDecreasedByOne && userGotRemoved);
	}
	
	@Test
	public void updateUserTest() {
		User user = service.findAllUsers().get(0);
		User tmp = new User("updatedNAME", "updatedSURNAME", "updatedNICKNAME", "updatedEMAIL", "updatedPAYPALEMAIL",
				"updatedPWD");
		
		user.update(tmp);
		service.createOrUpdateUser(user);
		
		User updatedUser = service.findUserById(user.getId());
		assertTrue(updatedUser.getEmail().equals(tmp.getEmail())
				&& updatedUser.getPayPalEmail().equals(tmp.getPayPalEmail())
				&& updatedUser.getPassword().equals(tmp.getPassword())
				&& updatedUser.getName().equals(tmp.getName())
				&& updatedUser.getSurname().equals(tmp.getSurname())
				&& updatedUser.getNickname().equals(tmp.getNickname())
				&& updatedUser.getEnabled().equals(tmp.getEnabled()));
	}
	
}
