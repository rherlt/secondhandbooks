package de.htw.secondhandbooks.integrationtest;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.imageio.ImageIO;

import de.htw.secondhandbooks.config.SecurityConfigurationTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.util.Assert;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import de.htw.secondhandbooks.config.PersistenceContextTest;
import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.service.BookService;
import de.htw.secondhandbooks.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { PersistenceContextTest.class, SecurityConfigurationTest.class})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:UserTestData.xml")
public class BookServiceTest {

	@Autowired
	private BookService bookService;
	@Autowired
	private UserService userService;

	// /path/to/project/src/test/resources/
	private final String PATH_TO_TEST_RESOURCES = System.getProperty("user.dir") + File.separatorChar + "src"
			+ File.separatorChar + "test" + File.separatorChar + "resources" + File.separatorChar;

	@Test
	public void createBookTest() {
		Book book = new Book("TestTitle", null, null,"3-499-13599-1","Richard Stalman");
		Assert.notNull(book);
		bookService.createBook(book);

		Collection<Book> books = bookService.findBooksByTitle("TestTitle");
		Assert.notEmpty(books);
	}

	@Test
	public void deleteBookTest() {
		Book book = bookService.findBookById((long) 1);
		Assert.notNull(book);

		bookService.deleteBook(book.getId());
		book = bookService.findBookById((long) 1);
		Assert.isNull(book);
	}

	@Test
	public void updateBookTest() {
		Book book = bookService.findBookById((long) 1);
		Assert.notNull(book);

		List<Book> books = bookService.findBooksByTitle("Atlas");
		assertEquals(0, books.size());

		book.setTitle("Atlas");
		bookService.updateBook(book);

		books = bookService.findBooksByTitle("Atlas");
		assertEquals(1, books.size());
	}

	@Test
	public void setImageTest() {
		Book book = bookService.findBookById((long) 1);
		Assert.notNull(book);
		Assert.isNull(book.getImage());

		String pathToImg = PATH_TO_TEST_RESOURCES + "TestImg.jpg";
		try {
			book.setImage(ImageIO.read(new File(pathToImg)));
		} catch (IOException e) {
			System.err.println("Error on loading test image.");
			e.printStackTrace();
		}

		bookService.updateBook(book);
		book = bookService.findBookById((long) 1);
		Assert.notNull(book.getImage());
	}
	
	@Test
	public void createBookForUserTest() {
		User user = new User("Mustermann", "Max", "Maxi", "max_mustermann@gmail.com", "max_mustermann@gmail.com",
				"pwd123");
		userService.createOrUpdateUser(user);
		
		Book book = new Book("TestTitle", null, user,"3-499-13599-1","Richard Stallman");
		bookService.createBook(book);
		
		Assert.notNull(book.getOwner());
	}
}
