package de.htw.secondhandbooks.integrationtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import de.htw.secondhandbooks.config.SecurityConfigurationTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import de.htw.secondhandbooks.config.PersistenceContextTest;
import de.htw.secondhandbooks.model.Book;
import de.htw.secondhandbooks.model.User;
import de.htw.secondhandbooks.service.BookService;
import de.htw.secondhandbooks.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { PersistenceContextTest.class, SecurityConfigurationTest.class})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:UserTestData.xml")
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private BookService bookService;

    private User user;
    private final String E_MAIL = "max_mustermann@gmail.com";

    @Before
    public void setUp() {
        user = new User("Mustermann", "Max", "Maxi", E_MAIL, E_MAIL, "pwd123");
        userService.createOrUpdateUser(user);
    }

    @After
    public void tearDown() {
        userService.deleteUser(user.getId());
    }

    @Test
    public void persistNewUserTest() {
        User user = userService.findUserByEmail(E_MAIL);

        assertNotNull(user);
        assertEquals(E_MAIL, user.getEmail());
    }

    @Test
    public void deleteUserTest() {
        Long userID = userService.findAllUsers().get(0).getId();
        int userCountBeforeDeletion = userService.findAllUsers().size();
        userService.deleteUser(userID);

        assertNotNull(userID);
        assertEquals(userCountBeforeDeletion - 1, userService.findAllUsers().size());
    }

    @Test
    public void updateUserTest() {
        User tmp = new User("updatedNAME", "updatedSURNAME", "updatedNICKNAME", "updatedEMAIL",
                "updatedPAYPALEMAIL", "updatedPWD");
        user.update(tmp);

        userService.createOrUpdateUser(user);

        User updatedUser = userService.findUserById(user.getId());

        assertEquals(tmp.getEmail(), updatedUser.getEmail());
        assertEquals(tmp.getPayPalEmail(), updatedUser.getPayPalEmail());
        assertEquals(tmp.getPassword(), updatedUser.getPassword());
        assertEquals(tmp.getName(), updatedUser.getName());
        assertEquals(tmp.getSurname(), updatedUser.getSurname());
        assertEquals(tmp.getNickname(), updatedUser.getNickname());
    }

    @Test
    public void userHasBooksTest() {
        Book book1 = new Book("test1", null, user,"3-499-13599-1","Richard Stallman");
        bookService.createBook(book1);
        Book book2 = new Book("test2", null, user,"3-411-13579-1","Linus Torvalds");
        bookService.createBook(book2);

        User user = userService.findUserByEmail(E_MAIL);
        assertNotNull(user);

        System.out.println(user.toString());

        assertEquals(2, user.getBooks().size());
        assertEquals("test1", user.getBooks().get(0).getTitle());
        assertEquals("test2", user.getBooks().get(1).getTitle());
    }

    @Test
    public void userHasNicknameTest(){
        assertEquals(user.getNickname() ,user.getDisplayName());
        assertEquals("Maxi" ,user.getDisplayName());
    }

    @Test
    public void userHasNoNicknameTest(){
        user = new User();
        user.setEmail(E_MAIL + "1");
        user.setPayPalEmail(E_MAIL + "1");
        user.setName("Max");
        user.setSurname("Mustermann");
        user.setPassword("123456");
        userService.createOrUpdateUser(user);

        assertEquals(user.getName() + " " + user.getSurname(), user.getDisplayName());
        assertEquals("Max Mustermann", user.getDisplayName());
    }
}
